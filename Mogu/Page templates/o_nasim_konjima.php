<?php
/*
 * Template Name: Predlozak O Nasim Konjima
 */
?>
<?php get_header(); ?>
<div class ="container">
    <div class="curvedBorder">
    <div class ="row">
        <div class ="col-md-12 col-lg-12 col-sm-12">
             <h2><b><div style ="color:<?php the_field( 'page_o_nasim_konjima_title_color'); ?>" ><?php the_field( 'page_o_nasim_konjima_title' ); ?></div></b></h2>
        </div>
    </div>
    </br>
    
    <div class ="row">
        <div class ="col-md-10 col-lg-10 col-sm-12 col-md-push-1">
            <?php if( get_field( 'page_o_nasim_konjima_frame_check') ) :
                if( in_array( 'Sa okvirom', get_field( 'page_o_nasim_konjima_frame_check') ) ) : ?>
                    <table cellpadding="50" align="center" style ="border:5px solid black"><tr><td align="center">
                <?php endif;
            endif; ?>
            <?php the_field( 'page_o_nasim_konjima_text' ); ?>
            <?php if( get_field( 'page_o_nasim_konjima_frame_check') ) :
                if( in_array( 'Sa okvirom', get_field( 'page_o_nasim_konjima_frame_check') ) ) : ?>
                    </td></tr></table>
                <?php endif;
            endif; ?>
        </div>
    </div>
    </br>
    </br>
    <div class ="row thumbnailMargin">
        <ul>
            <?php if( have_rows( 'page_o_nasim_konjima_horse_repeater' ) ) :
                while( have_rows( 'page_o_nasim_konjima_horse_repeater' ) ) :
                     the_row(); ?>
                     <?php if( get_sub_field ('page_o_nasim_konjima_horse_repeater_page') ) : ?>
                        <?php $horse = get_sub_field( 'page_o_nasim_konjima_horse_repeater_page'); ?>
                     <?php endif; ?>        
                    <?php $image = get_sub_field( 'page_o_nasim_konjima_horse_repeater_image' ); ?>
                    <li class ="thumbnail col-xs-2 col-sm-3 col-md-3 col-lg-3">
                        <a href ="<?php echo get_permalink( get_page_id( $horse ) ); ?>" target="blank" ><img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
                        <div class ="caption">
                            <h3><?php the_sub_field( 'page_o_nasim_konjima_horse_repeater_name' ); ?></h3>
                        </div>
                    </li>
                <?php endwhile;
            endif; ?>
        </ul>
    </div>
    <div class ="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <b>Kliknite na sliku konja za vise informacija</b>
        </div>
    </div>
    </div>
</div>
</br>


<?php get_footer(); ?>

