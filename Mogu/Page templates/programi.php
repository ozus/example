<?php
/*
 * Template Name: Predlozak Programi
 */
?>
<?php get_header(); ?>
<div class="container">
    <div class="curvedBorder">
    <div class ="row">
        <div class ="col-md-12 col-lg-12 col-sm-12">
             <h2><b><div style ="color:<?php the_field( 'page_programi_color'); ?>" ><?php the_field( 'page_programi_title' ); ?></div></b></h2>
        </div>
    </div>
    </br>
    <div class ="row">
        <div class ="col-md-12 col-lg-12 col-sm-12">
            <?php if( get_field( 'page_programi_frame_check') ) : ?>
                </br>
                </br>
                <?php if( in_array( 'Sa okvirom', get_field( 'page_programi_frame_check') ) ) : ?>
                    <table cellpadding="50" align="center" style ="border:5px solid black"><tr><td align="center">
                <?php endif;
            endif; ?>
            <?php the_field( 'page_programi_text' ); ?>
            <?php if( get_field( 'page_programi_frame_check') ) : ?>
                <?php if( in_array( 'Sa okvirom', get_field( 'page_programi_frame_check') ) ) : ?>
                    </td></tr></table>
               
                <?php endif;
            endif; ?>
        </div>
    </div>
    </br>
    <div class="row">
            <div class ="panel-group" id="accordion">
                <?php $counter = 0; ?>
                <?php if( have_rows( 'page_programi_repeater_program' ) ) :
                    while( have_rows( 'page_programi_repeater_program' ) ) :
                        the_row(); ?>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#<?php echo $counter; ?>">
                            <center><?php the_sub_field( 'page_programi_repeater_program_title' ); ?></center>
                        </a>
                      </h4>
                    </div>
                    <div id="<?php echo $counter; ?>" class="panel-collapse collapse">  
                      <div class="panel-body">
                        <?php the_sub_field( 'page_programi_repeater_program_content' ); ?>
                      </div>   
                    </div>
                <?php $counter++; ?>
                </div>
                    </br>    
                </div> 
                <?php endwhile;
                endif; ?>
            </div>
        </div>
    </div>
    </div>
</div>
<?php get_footer(); ?>
