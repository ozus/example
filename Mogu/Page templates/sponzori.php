<?php
/*
 * Template Name: Predlozak Sponzori
 */
?>
<?php get_header(); ?>
<div class="container">
    <div class="curvedBorder">
    <div class ="row">
        <div class ="col-md-12 col-lg-12 col-sm-12">
             <h2><b><div style ="color:<?php the_field( 'page_sponzori_color'); ?>" ><?php the_field( 'page_sponzori_title' ); ?></div></b></h2>
        </div>
    </div>
    </br>
    <?php if( get_field( 'page_sponzori_text' ) ) : ?>
    <div class ="row">
        <div class ="col-md-12 col-lg-12 col-sm-12">
            <?php if( get_field( 'page_sponzori_frame_check') ) : ?>
                </br>
                </br>
                <?php if( in_array( 'Sa okvirom', get_field( 'page_sponzori_frame_check') ) ) : ?>
                    <table cellpadding="50" align="center" style ="border:5px solid black"><tr><td align="center">
                <?php endif;
            endif; ?>
            <?php the_field( 'page_sponzori_text' ); ?>
            <?php if( get_field( 'page_sponzori_frame_check') ) : ?>
                <?php if( in_array( 'Sa okvirom', get_field( 'page_sponzori_frame_check') ) ) : ?>
                    </td></tr></table>
                <?php endif;
            endif; ?>
        </div>
    </div>
    </br>
    <?php endif; ?>
    <div class="row">
            <?php if( have_rows( 'page_sponzori_repeater_sponzors' ) ) :
                while( have_rows( 'page_sponzori_repeater_sponzors' ) ) :
                    the_row(); ?>
                    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
                        <?php $image = get_sub_field( 'page_sponzori_repeater_sponzors_logo' ); ?>
                        <a href="<?php the_sub_field( 'page_sponzori_repeater_sponzors_link' ); ?>" target="blank"><img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['title']; ?>" /></a>
                        <p><h4><?php the_sub_field( 'page_sponzori_repeater_sponzors_name' ); ?></h4></p>
                    </div>
                <?php endwhile;
            endif; ?> 
    </div>
    </br>
    </br>
    </br>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <button type="button" class="btn btn-danger" data-toggle="collapse" data-target="#demo">Postanite sponzor</button>
            <div id="demo" class="collapse">
                </br>
                <table style="border:1px solid black"><tr><td bgcolor="#ffffff">
                <table>
                    <tr>
                        <td style="width:572px;" bgcolor="#ffffff">
                            <h4><b>PRIVATNE OSOBE</b></h4>
                        </td>
                        <td bgcolor="#ffffff"></td>
                        <td bgcolor="#ffffff"></td>
                    </tr>
                </table>    
                <form action="<?php echo TEMPLATE_URI; ?>/sponzor_private.php" method="get">
                    <table cellpadding="10" >
                        <tr>
                            <td style="padding:10px;" bgcolor="#ffffff">
                                Ime:</br><input type="text" name="name">
                            </td>
                            <td style="padding:10px;" bgcolor="#ffffff">
                                Prezime:</br><input type="text" name="Lname">
                            </td>
                            <td style="padding:10px;" bgcolor="#ffffff"></td>
                        </tr>
                        <tr>
                            <td style="padding:10px;" bgcolor="#ffffff">
                                Adresa:</br><input type="text" name="adress">
                            </td>
                            <td style="padding:10px;" bgcolor="#ffffff">
                                Grad:</br><input type="text" name="city">
                            </td>
                            <td style="padding:10px;" bgcolor="#ffffff">
                                Država:</br> <input type="text" name="state">
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:10px;" bgcolor="#ffffff">
                                Tel/Mob:</br> <input type="tel" name="phone">
                            </td>
                            <td style="padding:10px;" bgcolor="#ffffff">
                                E-mail:</br> <input type="email" name="mail">
                            </td>
                            <td style="padding:10px;" bgcolor="#ffffff"></td>
                        </tr>
                    </table>
                    <table cellpadding="10">
                        <tr>
                            <td bgcolor="#ffffff">
                                Vaša poruka:</br><textarea type="text" name="massage" rows="8" cols="100"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#ffffff">
                                <input type="submit" value="Pošalji">
                            </td>
                        </tr> 
                    </table>
                    </td></tr></table>
                </form>
                </br>
                <table style="border:1px solid black"><tr><td bgcolor="#ffffff">
                <table>
                    <tr>
                        <td style="width:572px;" bgcolor="#ffffff">
                            <h4><b>PRAVNE OSOBE</b></h4>
                        </td>
                        <td bgcolor="#ffffff"></td>
                        <td bgcolor="#ffffff"></td>
                    </tr>
                </table>    
                <form action="<?php echo TEMPLATE_URI; ?>/sponzor_public.php" method="get">
                    <table cellpadding="10">
                        <tr>
                            <td bgcolor="#ffffff">
                                Ime institucije:<input type="text" name="name">
                            </td>
                            <td bgcolor="#ffffff">
                                Predstavnik: <input type="text" name="repres"
                            </td>
                            <td bgcolor="#ffffff"></td>
                            <td bgcolor="#ffffff"></td>
                        </tr>
                        <tr>
                            <td bgcolor="#ffffff">
                                Sjedište:<input type="text" name="adress">
                            </td>
                            <td bgcolor="#ffffff">
                                Grad:<input type="text" name="city">
                            </td>
                            <td bgcolor="#ffffff">
                                Država: <input type="text" name="state">
                            </td>
                            <td bgcolor="#ffffff"></td>
                        </tr>
                        <tr>
                            <td bgcolor="#ffffff">
                                Kontakt1: <input type="tel" name="phone">
                            </td>
                            <td bgcolor="#ffffff">
                                Kontakt2: <input type="tel" name="phone2">
                            </td>
                            <td bgcolor="#ffffff">E-mail1: <input type="email" name="mail"></td>
                            <td bgcolor="#ffffff">E-mail2: <input type="email" name="mail2"></td>
                        </tr>
                    </table>
                    <table cellpadding="10">
                        <tr>
                            <td bgcolor="#ffffff">
                                Vaša poruka:</br><textarea type="text" name="massage" rows="8" cols="100"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#ffffff">
                                <input type="submit" value="Pošalji">
                            </td>
                        </tr> 
                    </table>
                    </td></tr></table>
                </form>
            </div>
        </div>
    </div>
    </div>
</div>

<?php get_footer(); ?>
