<?php
/*
 * Template Name: Predlozak Sto Cinimo
 */
?>
<?php get_header(); ?>
<div class ="container">
    <div class="curvedBorder1">
    <div class ="row">
        <div class ="col-md-12 col-lg-12 col-sm-12">
             <h2><b><div style ="color:<?php the_field( 'page_sto_cinimo_title_color'); ?>" ><?php the_field( 'page_sto_cinimo_title' ); ?></div></b></h2>
        </div>
    </div>
    </br>
    <div class ="row">
        
            <?php 
                if( have_rows( 'page_sto_cinimo_repeater_images_top' ) ) :
                    while( have_rows( 'page_sto_cinimo_repeater_images_top' ) ) :
                        the_row(); ?>
                        <div class ="col-md-4 col-lg-4 col-sm-6 col-xs-6">
                            <?php $image_top = get_sub_field( 'page_sto_cinimo_repeater_images_top_image'); ?>
                            <a href="<?php echo $image_top['url']; ?>" target ="blank" ><img src="<?php echo $image_top['sizes']['medium']; ?>" alt="<?php echo $image_top['title']; ?>" /></a>
                        </div>
                    <?php endwhile;
                endif; 
            ?>
        
    </div>
    <div class ="row">
        <div class ="col-md-10 col-lg-10 col-sm-12 col-md-push-1">
            <?php if( get_field( 'page_sto_cinimo_frame_check') ) : ?>
                </br>
                </br>
                <?php if( in_array( 'Sa okvirom', get_field( 'page_sto_cinimo_frame_check') ) ) : ?>
                    <table cellpadding="50" align="center" style ="border:5px solid black"><tr><td align="center">
                <?php endif;
            endif; ?>
            <?php the_field( 'page_sto_cinimo_text' ); ?>
            <?php if( get_field( 'page_sto_cinimo_frame_check') ) : ?>
                <?php if( in_array( 'Sa okvirom', get_field( 'page_sto_cinimo_frame_check') ) ) : ?>
                    </td></tr></table>
                </br>
                </br>
                <?php endif;
            endif; ?>
        </div>
    </div>
    <div class ="row">
        
            <?php 
                if( have_rows( 'page_sto_cinimo_repeater_images_bottom' ) ) :
                    while( have_rows( 'page_sto_cinimo_repeater_images_bottom' ) ) :
                        the_row(); ?>
                        <div class ="col-md-4 col-lg-4 col-sm-6 col-xs-6">
                            <?php $image_bottom = get_sub_field( 'page_sto_cinimo_repeater_images_bottom_image'); ?>
                            <a href="<?php echo $image_bottom['url']; ?>" target ="blank" ><img src="<?php echo $image_bottom['sizes']['medium']; ?>" alt="<?php echo $image_bottom['title']; ?>" /></a>
                        </div>
                    <?php endwhile;
                endif; 
            ?>
       
    </div>
    </br>
    </div>
</div>
<?php get_footer(); ?>

