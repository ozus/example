<?php
/*
 * Template Name: Predlozak Terapijsko Jahanje
 */
?>
<?php get_header(); ?>
<div class ="container">
    <div class="curvedBorder">
    <div class ="row">
        <div class ="col-md-12 col-lg-12 col-sm-12">
             <h2><b><div style ="color:<?php the_field( 'page_terapijsko_jahanje_color'); ?>" ><?php the_field( 'page_terapijsko_jahanje_title' ); ?></div></b></h2>
        </div>
    </div>
    </br>
     <div class ="row">
            <?php
                if( have_rows( 'page_terapijsko_jahanje_repeater_sadrzaj' ) ) :
                    while( have_rows( 'page_terapijsko_jahanje_repeater_sadrzaj' ) ) :
                        the_row(); ?>
                        <div class ="col-md-12 col-lg-12 col-sm-6">
                            <?php if( get_sub_field( 'page_terapijsko_jahanje_repeater_sadrzaj_frame_check') ) : ?>
                                <?php if( in_array( 'Sa okvirom', get_sub_field( 'page_terapijsko_jahanje_repeater_sadrzaj_frame_check') ) ) : ?>
                                    <table cellpadding="50" align="center" style ="border:5px solid black"><tr><td align="center">
                                <?php endif;
                            endif; ?>
                            <?php the_sub_field( 'page_terapijsko_jahanje_repeater_sadrzaj_content' ); ?>
                            <?php if( get_sub_field( 'page_terapijsko_jahanje_repeater_sadrzaj_frame_check') ) : ?>
                                <?php if( in_array( 'Sa okvirom', get_sub_field( 'page_terapijsko_jahanje_repeater_sadrzaj_frame_check') ) ) : ?>
                                    </td></tr></table>
                                <?php endif;
                            endif; ?>
                            </br>
                            </br>
                        </div>    
                    <?php endwhile;
                endif;
            ?>  
    </div>
    </div>
    </br>
    <div class="curvedBorder">
        <div class ="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <h4><b>Više o terapijskom jahanju:</b></h4>
            </div>
        </div>    
            </br>
         <div class="row">   
            <?php if(have_rows( 'page_terapijsko_jahanje_reperater_links' ) ) :
                while( have_rows( 'page_terapijsko_jahanje_reperater_links' ) ) :
                    the_row(); ?>
                    <div class ="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <a href="<?php the_sub_field( 'page_terapijsko_jahanje_reperater_links_link' ); ?>" class ="LinkButton" target="blank" ><?php the_sub_field( 'page_terapijsko_jahanje_reperater_links_name' ); ?></a>
                    </div>
                <?php endwhile;
            endif; ?>
        </div>
            </br>
    </div>
</div>
<?php get_footer(); ?>

