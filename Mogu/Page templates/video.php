<?php
/*
 * Template Name: Predlozak Video
 */
?>
<?php get_header(); ?>
<div class ="container">
    <div class="curvedBorder">
        <div class="inner">
    <div class ="row">
        <div class ="col-md-12 col-lg-12 col-sm-12">
             <h2><b><div style ="color:<?php the_field( 'page_video_title_color'); ?>" ><?php the_field( 'page_video_title' ); ?></div></b></h2>
        </div>
    </div>
    </br>
    <div class ="row">
        <div class ="col-md-12 col-lg-12 col-sm-12">
            <?php if( get_field( 'page_video_frame_check') ) : ?>
                </br>
                
                <?php if( in_array( 'Sa okvirom', get_field( 'page_video_frame_check') ) ) : ?>
                    <table cellpadding="50" align="center" style ="border:5px solid black"><tr><td align="center">
                <?php endif;
            endif; ?>
            <?php the_field( 'page_video_text' ); ?>
            <?php if( get_field( 'page_video_frame_check') ) : ?>
                <?php if( in_array( 'Sa okvirom', get_field( 'page_video_frame_check') ) ) : ?>
                    </td></tr></table>
                <?php endif;
            endif; ?>
        </div>
    </div>
    </br>
    </br>
    <button type="button" class="btn btn-danger" data-toggle="collapse" data-target="#demo">Navigacija</button>
    <div style="position:relative;" class="row aligncenter" data-spy="scroll" data-target=".navbar-video">
        <?php $counter = 0; ?>
        <div id="demo" class="collapse">
        <table>
            <tr>
                <td style="padding:10px" align="center">
                    <div class="navbar-video">
                        <ul class="nav navbar-nav">
                            <?php if( have_rows( 'page_video_repeater_add_video' ) ) :
                                while( have_rows( 'page_video_repeater_add_video' ) ) :
                                    the_row(); ?>

                            <li class=" videoButton col-md-2"> <a href="#<?php echo $counter; ?>" ><?php the_sub_field( 'page_video_repeater_add_video_title' ); ?></a></li>

                                    <?php $counter++; ?>

                                <?php endwhile;
                            endif; ?>
                        </ul>
                    </div>
                </td>
            </tr>
        </table>    
        </div>
        <?php $counter1 = 0; ?>
        <table>
            <tr>
                <td align="center" style="padding:30px">
                    <?php if( have_rows( 'page_video_repeater_add_video' ) ) :
                           while( have_rows( 'page_video_repeater_add_video' ) ) :
                               the_row(); ?>
                               <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                   <div id="<?php echo $counter1; ?>">
                                       <p><b><h4><?php the_sub_field( 'page_video_repeater_add_video_title'); ?></h4></b></p>    
                                       <p> <?php the_sub_field( 'page_video_repeater_add_video_video' ); ?></p>
                                       <p><b><?php the_sub_field( 'page_video_repeater_add_video_desc' ); ?></b></p>
                                   </div>
                                   </br>
                                   </br>
                                   </br>
                               </div>
                               <?php $counter1++; ?>

                           <?php endwhile;
                    endif; ?>
                </td>
            </tr>
        </table>    
    </div>
    </div>
    
    </div>
</div>
<?php get_footer(); ?>

