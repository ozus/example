<?php
/*
 * Template Name: Predlozak Dodaj sliku
 */
?>

<?php get_header( 'volonters' ); ?>
<?php if( !isset( $_SESSION['user']) || !isset( $_SESSION['password'] ) ) : ?>
<table align="center" style="width:500px; height:400px; border:3px solid black">
    <tr>
        <td style="padding:10px;" align="center" bgcolor="#ffffff">
            <b>Sadržaj ove stranice mogu vidjeti samo logirani korisnici, molimo vas logirajte se ili se registriajte ako nemate account na stranici.</b>
        </td>
    </tr>
    <tr>
        <td bgcolor="#C0C0C0" valign="middle">
            <table style="width:500px;">
                <tr>
                    <td align="center" style="padding:10px;">
                        <a href="<?php echo TEMPLATE_URI; ?>/login/login.php" class="LinkButton1">Login</a></br>
                    </td>
                    <td align="center" style="padding:10px;">
                        <a href="<?php echo TEMPLATE_URI; ?>/login/register.php" class="LinkButton1">Registracija</a>
                    </td>
                </tr>
            </table>    
        </td>
    </tr>
</table>    
<?php else : ?>
    
<div class="container">
    <div class="row">
        <div class ="col-xs-12 col-sm-12 col-md-6 col-lg-6 curvedBorder3">
            <?php if( get_field( 'page_volonteri_slike_frame_check') ) : ?>
                <?php if( in_array( 'Sa okvirom', get_field( 'page_volonteri_slike_frame_check') ) ) : ?>
                    <table cellpadding="50" align="center" style ="border:5px solid black"><tr><td align="center">
                <?php endif;
            endif; ?>
            <b><?php the_field( 'page_volonteri_slike_text' ); ?></b>
            <?php if( get_field( 'page_volonteri_slike_frame_check') ) : ?>
                <?php if( in_array( 'Sa okvirom', get_field( 'page_volonteri_slike_frame_check') ) ) : ?>
                    </td></tr></table>
                <?php endif;
            endif; ?>
        </div>
    
    
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-md-push-1 col-lg-push-1">    
            <form action="<?php echo TEMPLATE_URI; ?>/UploadImage/UploadImage.php" method="post" enctype="multipart/form-data">
                <label for="image">Dodaj sliku:</label>
                <input type="file" name="image" id="image"><br>
                <label for="image">Ime slike:</label><br>
                <input type="text" name="name"><br>
                <label for="area">Opis slike:</label><br>
                <textarea name="area" rows="6" cols="50"></textarea><br>
                <input type="submit" name="submit" value="Uploadaj">
            </form>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php get_footer( 'volonters' ); ?>

