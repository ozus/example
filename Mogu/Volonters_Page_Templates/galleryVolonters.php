<?php session_start(); ?>
<?php
/*
 * Template Name: Predlozak Volonteri Galerija
 */
?>
<?php get_header( 'volonters' ); ?>
<?php
$db= mysqli_connect("localhost", "root", "", "mogu");
If(mysqli_connect_errno()) {
   exit("Neuspjelo spajanje na bazu" . mysqli_connect_error());
}
$sql_query = 'SELECT* From gallery';
$result = mysqli_query($db, $sql_query); ?>

    <div class="container">
      <div class="row">  
        <div class="row">
            <?php $counter = 0; ?>
            <?php $counter1 = 1000000000; ?>
            <?php while($row = mysqli_fetch_array($result)) : ?>
            <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
              <p><table>
                    <tr>
                        <td style="padding:5px; width:300px; border-bottom:2px solid black">
                            <?php echo $row['title']; ?>
                        </td>
                        <td style="padding:5px; border-bottom:2px solid black; width:70px" align="center">
                            <form action="<?php echo TEMPLATE_URI; ?>/login/user.php" method="post">
                                    <input type="hidden" name="button" value="<?php echo $row['author']; ?>">
                                    <input type="submit" class="LinkButton2" name="submit" value="<?php echo $row['author']; ?>">
                                </form>
                        </td>
                    </tr>
                </table></p>  
                             
                <p><a href="#x" data-toggle="modal" data-target="#<?php echo $counter1; ?>" ><img class="thumbnail" src="<?php echo TEMPLATE_URI . "/UploadImage/upload/" . $row['img']; ?>"  alt="Slika" width="230" height="200" /></a></p>
                
                <p><button type="button" class="btn btn-danger" data-toggle="collapse" data-target="#<?php echo $counter; ?>">Prikazi opis</button>
                    <div id="<?php echo $counter; ?>" class="collapse">
                        <table>
                            <tr>
                                <td style="padding:10px;" bgcolor="#ffffff">        
                                    <p><?php echo $row['description']; ?></p>
                                </td>
                            </tr>
                        </table>    
                    </div>
                </p>
                <p>
                                    <form action='<?php echo TEMPLATE_URI ?>/GradeImage.php' method='get'>
                                        Ocijeni Video:<input type='text' name='text' value='<?php echo $row['title']; ?>'>
                                          <select name='grade'>
                                            <option value='1'>1</option>
                                            <option value='2'>2</option>
                                            <option value='3'>3</option>
                                            <option value='4'>4</option>
                                            <option value='5'>5</option>
                                            <option value='6'>6</option>
                                            <option value='7'>7</option>
                                            <option value='8'>8</option>
                                            <option value='9'>9</option>
                                            <option value='10'>10</option>
                                        </select>
                                        <input type='submit' name='submit' value='Ocijeni'>
                                    </form>
                </p>              
                    <p><b>Ocijena:</b><h3><b><div style='color:#FF0000;'><?php echo $row['grade']; ?></div></b></h3></p>
            </div>
            <div class="modal fade" id="<?php echo $counter1; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title" id="myModalLabel"><?php echo $row['title']; ?></h4>
                        </div>
                        <div class="modal-body">
                            <p><img src="<?php echo TEMPLATE_URI; ?>/UploadImage/upload/<?php echo $row['img']; ?>" alt="slika" width="1024" height="780" class="img-responsive" /></p>
                            <p><?php echo $row['description']; ?></p>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php $counter++; ?>
                  <?php $counter1++; ?>
            <?php endwhile; ?>        
        </div>
      </div>
    </div>
<?php mysqli_close($db); ?>
<?php get_footer( 'volonters' ); ?>

