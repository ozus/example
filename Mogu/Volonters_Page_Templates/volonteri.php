<?php
/*
 * Template Name: Predlozak Volonteri
 */
?>
                                <?php
                                $name = $_GET['nname'];
                                $lname = $_GET['lname'];
                                $contact = $_GET['tel'];
                                $mail = $_GET['mail'];
                                $mess = $_GET['message'];
                                $hidd = $_GET['hidden'];
                                if( $hidd == 'yes') :
                                ?>
                                <?php
                                require(TEMPLATE_PATH . "/lib/fpdf/fpdf.php");
                                $pdf=new FPDF('P','mm','Letter');
                                $pdf->AddPage();
                                $pdf->SetFont('Arial','B','15');
                                $pdf->Cell(0,0,'Vaša Prijava',0,1,'C');
                                $pdf->ln(60);
                                $pdf->Cell(0,0,'Vaše ime:'.$name,0,0,'L');
                                $pdf->Cell(0,0,'Vaše prezime:'.$lname,0,1,'R');
                                $pdf->ln(30);
                                $pdf->Cell(0,0,'Kontakt:'.$contact,0,0,'L');
                                $pdf->Cell(0,0,'E-mail:'.$mail,0,1,'R');
                                $pdf->ln(30);
                                $pdf->Cell(0,0,'Vaša poruka:'.$mess,0,1,'L');
                                $pdf->ln(60);
                                $pdf->Cell(0,0,'Vaš potpis:',0,1,'R');
                                $pdf->Cell(50,5,'',0,1,'R');
                                $pdf->Output('C:\xampp\htdocs\wordpress\wp-content\themes\Mogu\PDF/Volonteri/' . $name . '_' . $lname . '.pdf', 'F'); 
                                ?>
                                <?php endif; ?>
<?php get_header( 'volonters' ); ?>
<div class="container">
    <div class ="row">
        <div class ="col-md-12 col-lg-12 col-sm-12">
             <h3><b><div style ="color:<?php the_field( 'page_volonteri_title_color'); ?>" ><?php the_field( 'page_volonters_title' ); ?></div></b></h3>
        </div>
    </div>
    </br>
    <div class="row"> 
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="well">
        <div id="myCarousel" class="carousel slide">    
            <div class="carousel-inner">
                <?php 
                $counter = 0; 
                ?>
                 <?php if( have_rows( 'page_volonters_repeater_carousel' ) ) :
                while( have_rows( 'page_volonters_repeater_carousel' ) ) :
                    the_row(); ?>
                    <?php if( $counter == 0) : ?>
                        <div class="item active">
                            <div class="row">
                   <?php endif; ?>
                   <?php if( $counter == 6 || $counter == 12 || $counter == 18 || $counter == 24 || $counter == 30 || $counter == 36 || $counter == 42 || $counter == 48 || $counter == 54 || $counter == 60 || $counter == 66) : ?>
                        <div class="item">
                            <div class="row">
                   <?php endif; ?>
                           <?php $image = get_sub_field( 'page_volonters_repeater_carousel_image' ); ?>    
                                <div class="col-sm-2"><a href="#x" data-toggle="modal" data-target="#<?php echo $counter; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>" width="250" height="250" class="img-responsive"></a>
                            </div>                            
                   <?php if( $counter == 5) : ?>
                        </div>
                            </div>   
                   <?php endif; ?> 
                   <?php if( $counter == 11 || $counter == 17 || $counter == 23 || $counter == 29 || $counter == 35 || $counter == 41 || $counter == 47 || $counter == 53 || $counter == 59 || $counter == 65 || $counter == 71) : ?>
                        </div>
                            </div>
                   <?php endif; ?>
                    <?php 
                    $counter++;
                    ?>
                <?php endwhile;
                endif; ?>
            </div>
            <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
        </div> 
    </div>
    </div>
    </div>
    <?php $counter1 = 0; ?>
            <?php if( have_rows( 'page_volonters_repeater_carousel' ) ) :
                while( have_rows( 'page_volonters_repeater_carousel' ) ) :
                    the_row(); ?>
                    <div class="modal fade" id="<?php echo $counter1; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title" id="myModalLabel"><?php the_sub_field( 'page_volonters_repeater_carousel_name' ); ?></h4>
                        </div>
                        <div class="modal-body">
                            <?php $image1 = get_sub_field( 'page_volonters_repeater_carousel_image' ); ?> 
                            <p><img src="<?php echo $image1['sizes']['large']; ?>" alt="slika" class="img-responsive pull-left" /></p>
                            <p><b><?php the_sub_field( 'page_volonters_repeater_carousel_text' ); ?></b></p>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php $counter1++; ?>
                <?php endwhile;
            endif; ?>
    </br>
    <div class ="row">
        <div class ="col-md-8 col-lg-8 col-sm-8">
            <?php if( get_field( 'page_volonters_frame_check') ) : ?>
                </br>
                </br>
                <?php if( in_array( 'Sa okvirom', get_field( 'page_volonters_frame_check') ) ) : ?>
                    <table cellpadding="50" align="center" style ="border:5px solid black"><tr><td align="center">
                <?php endif;
            endif; ?>
            <?php the_field( 'page_volonteri_text' ); ?>
            <?php if( get_field( 'page_volonters_frame_check') ) : ?>
                <?php if( in_array( 'Sa okvirom', get_field( 'page_volonters_frame_check') ) ) : ?>
                    </td></tr></table>
                <?php endif;
            endif; ?>
                </br>
                </br>
                <p><h3><span style="color:#FF0000;" ><?php the_field( 'page_volonters_2nd_part_title' ); ?></span></h3></p>               
                <p><?php the_field( 'page_volonters_2nd_part_text' ); ?></p>
                </br>
                <h4>Pišite nam ili nas pitajte sve što vas zanima :)</h4>
                <table border="0" bgcolor="#ffffff" style="width:200px"><tr><td style="padding:10px;" bgcolor="#ffffff">
                            <form action="<?php echo TEMPLATE_URI; ?>/mail.php" method="post">
                                <label for="mail">E-mail:</label></br>
                                <input type="email" name="mail"></br></br>
                                <label for="message">Vaša Poruka/Pitanje:</label></br>
                                <textarea rows="8" cols="80" name="message"></textarea>
                                <input type="submit" name="submit" value="Pošalji">
                            </form>
                </td></tr></table> 
                </br>
                    <p><h4>Ili ste već sigruni?</h4></p>
                    <p>U tom slučaju ostavite nam vaše podatke i brzo čemo vam se javiti, i naravno veselimo se vašem dolasku:)</p>
                    <p>
                    <table border="0" bgcolor="#ffffff" style="width:200px"><tr><td style="padding:10px;" bgcolor="#ffffff"> 
                        <form action="<?php echo home_url() ?>" method="get">
                            <input type='hidden' name='page_id' value='<?php echo get_page_id("Volonteri"); ?>'></br>
                            <label for="nname">Ime<span style="color:#FF0000;">*</span>:</label></br>
                            <input type="text" name="nname"></br></br>
                            <label for="lname">Prezime<span style="color:#FF0000;">*</span>:</label></br>
                            <input type="text" name="lname"></br></br>
                            <label for="tel">Kontakt(tel/mob)<span style="color:#FF0000;">*</span>:</label></br>
                            <input type="text" name="tel"></br></br>
                            <label for="mail">E-mail:</label></br>
                            <input type="email" name="mail"></br></br>
                            <label for="message">Recite nam zašto želite biti volonter u našoj udruzi?<span style="color:#FF0000;">*</span>:</label></br>
                            <textarea cols="80" rows="8" name="message"></textarea></br></br>
                            <input type="hidden" name="hidden" value="yes">
                            <input type="submit" name="submit" value="Pošalji"></br></br>
                        </form>
                                Polja označena zvjezdicom je obavezno ispuniti.       
                    </td></tr></table>            
                    </p>
                    <?php   
                    $name = $_GET['nname'];
                    $lname = $_GET['lname'];
                    $contact = $_GET['tel'];
                    $mail = $_GET['mail'];
                    $mess = $_GET['message'];
                    $hidd = $_GET['hidden'];
                    if( $hidd == "yes" ):
                        if( !$name || !$lname || !$contact || !$mess ) :
                            echo "Molimo vas da unesete sve potrebne podatke";
                        endif;
                        if( $name && $lname && $contact && $mess && $name != "" && $lname != "" && $mail != "" && $mess != "" ) : 
                                $db = mysqli_connect("localhost", "root", "", "mogu");
                                If(mysqli_connect_errno()) :
                                    echo "Neuspjelo spajanje na bazu" . mysqli_connect_error;
                                endif;
                                $name1 = mysqli_real_escape_string( $db, $name );
                                $lname1 = mysqli_real_escape_string( $db, $lname );
                                $contact1 = mysqli_real_escape_string( $db, $contact );
                                $mail1 = mysqli_real_escape_string( $db, $mail );
                                $mess1 = mysqli_real_escape_string( $db, $mess );
                                $sql = "INSERT INTO volonters (name,lastname,contact,email,message) VALUES ('$name1','$lname1','$contact1','$mail1','$mess1')";
                                if( $result = mysqli_query( $db, $sql ) ) :
                                    echo "Podatci su uspješno poslani";
                                else :
                                    echo "Slanje podataka nije bilo uspješno";
                                endif;
                                mysqli_close( $db ); ?>
                                
                       <?php endif;
                    endif;
                    ?>
        </div>
        <div class ="col-md-4 col-lg-4 col-sm-4">
            <h3>Najnovije obavijesti</h3>
            <?php
                                query_posts( array(
                                    'post_type' => 'post',
                                    'category_name' => 'volonteri',
                                    'posts_per_page' => '5'
                                ));
                                if( have_posts() ) : while( have_posts() ) : the_post();
                            ?>
                                            <p><table border="0" bgcolor="#1E90FF" style="width:300px"><tr><td style="padding:10px;" bgcolor="#1E90FF">
                                            <h4><b><div style="color:#000000; text-decoration:none"><a href ="<?php echo get_permalink(); ?>" target="blank"><?php the_title(); ?></a></div></b></h4>
                                                    </td></tr></table></p>
                                            
                                <?php endwhile; ?>
                                <a href="<?php echo get_permalink(get_page_id( 'Arhiva Volonteri' ) ); ?>">Pogledaj sve obavijesti</a>
                                <?php endif; ?>
                                </br>
                                </br>
                                <table border="0" bgcolor="#1E90FF" style="width:300px"><tr><td style="padding:5px;" bgcolor="#1E90FF">
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <h3>Najnoviji video</h3> 
                                <?php $db = mysqli_connect("localhost", "root", "", "mogu");
                                If(mysqli_connect_errno()) :
                                    echo "Neuspjelo spajanje na bazu" . mysqli_connect_error;
                                endif;
                                $sql_query = "SELECT vid, title FROM video ORDER BY date DESC LIMIT 5";
                                $result = mysqli_query($db, $sql_query); ?> 
                                
                                <?php while($row = mysqli_fetch_array($result)) : ?>
                                     <p>
                                     <p><a href="<?php echo get_permalink(get_page_id( 'Gledanje Videa' ) ); ?>"><font size="2"><?php echo $row['title']; ?></font></a></p>
                                        <p><embed width="100" height="80"
                                        src="<?php echo $row['vid']; ?>"
                                        type="application/x-shockwave-flash"></p>
                                    </p>
                                <?php endwhile; ?>
                                       
                                <?php mysqli_close($db); ?>   
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-md-push-1">
                                    <h3>Najnovije slike</h3>
                                    <?php $db = mysqli_connect("localhost", "root", "", "mogu");
                                    If(mysqli_connect_errno()) :
                                        echo "Neuspjelo spajanje na bazu" . mysqli_connect_error;
                                    endif; ?>
                                     <?php $sql_query1 = "SELECT img, title FROM gallery ORDER BY date DESC LIMIT 5";
                                     $result1 = mysqli_query($db, $sql_query1); 
                                     while($row = mysqli_fetch_array($result1)) : ?>
                                        <p>
                                            <p><a href="<?php echo get_permalink(get_page_id( 'Gallery' ) ); ?>"><font size="2"><?php echo $row['title']; ?></font></a></p>
                                            <p><img src="<?php echo TEMPLATE_URI; ?>/UploadImage/Upload/<?php echo$row['img']; ?>" width="100" height="80" /></p>
                                        </p>
                                    <?php endwhile; ?>
                                    <?php mysqli_close($db); ?>    
                                </div>
                                  </td></tr></table>
                                </br>
                                <table border="0" bgcolor="#1E90FF" style="width:300px"><tr><td style="padding:5px;" bgcolor="#1E90FF">
                                <form action="<?php echo get_permalink(get_page_id('Anketa')) ?>" method="get">                        
                                    <table border="0" bgcolor="#00FFFF" style="width:290px"><tr><td style="padding:5px;" bgcolor="#00BFFF">
                                        <label>Koja je vaša omiljena aktivnost u Udruzi Mogu?</label>
                                        <table border="0" bgcolor="#00FFFF" style="width:270px"><tr><td style="padding:5px;" bgcolor="#87CEFA">
                                            <input type="hidden" name="page_id" value="<?php get_page_id('Anketa'); ?>">      
                                            <b><input type="radio" name="jobs" value="Čišćenje konja"> Čišćenje konja</b><br><br>
                                            <b><input type="radio" name="jobs" value="Sudjelovanje u terapiji"> Sudjelovanje u terapiji</b><br><br>
                                            <b><input type="radio" name="jobs" value="Briga za potrebe konja(štale, voda, hrana..)"> Briga za potrebe konja(štale, voda, hrana..)</b><br><br>
                                            <b><input type="radio" name="jobs" value="Jahanje"> Jahanje</b><br><br>
                                        </td></tr></table>
                                        </br>
                                        <input type="submit" name="submit" value="Odgovori">
                                    </td></tr></table></br>
                                </form><form action="<?php echo get_permalink(get_page_id('Anketa')) ?>" method="post"><input type="submit" value="Pogledaj rezultate"></form>
                                </td></tr></table>            
        </div>
    </div>
    </br>
    
</div>
<script>
$(document).ready(function() {
	$('#myCarousel').carousel({
	interval: 10000
	})
    
    $('#myCarousel').on('slid.bs.carousel', function() {
    	//alert("slid");
	});
    
    
});
</script>
<?php get_footer( 'volonters' ); ?>

