</br>
<center><h4>Komentari...</h4></center>
<?php if ( have_comments() ) : ?>
<?php
if ( get_comment_pages_count() > 1 && get_option( 'page_comments' )) : ?>
<table bgcolor="#000000" style="border:2px black solid; width:400px;">
    <tr>
        <td align="center" bgcolor="#00FFFF " style="padding:5px">
            <?php 
            previsu_comments_link( '<<Stariji Komentari' );
            next_comments_link( 'Noviji Komentari>>' );
            endif; ?>
        </td>
    </tr>
</table>    
<table border="0" cellspacing="10" bgcolor="#000000" style="width:400px;">
    <tr>
        <td align="left" bgcolor="#00FFFF " style="border-bottom:3px black solid; padding:5px">
            <ol>
                <?php wp_list_comments( array(
                    'stayle' => 'ol',
                    'size' => 54,
                    'per_page' => '10',
                    'short_ping' => true,
                    )); ?>
            </ol>
        </td>
    </tr>
</table>    
 <?php endif;  //have comments ?> 
<table border="0" cellspacing="10" bgcolor="#000000" style="width:400px;">
    <tr>
        <td align="left" bgcolor="#00FFFF " style="padding:5px">
            <?php comment_form(); ?>
        </td>
    </tr>
</table>
    