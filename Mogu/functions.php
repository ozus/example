<?php

function scripts_settings() {
    wp_deregister_script('jquery');
    wp_register_script('jquery', '');
    wp_enqueue_script('jquery');
}

add_action('wp_enqueue_scripts', 'scripts_settings');

//defining TEMPLATE_URI cause this path is used a lot
define('TEMPLATE_URI', get_template_directory_uri());
define('TEMPLATE_PATH', get_template_directory());

function get_page_url($name) {
    $page = get_page_by_title($name);
    return get_permalink($page->ID);
}



function get_page_id($name) {
    $page = get_page_by_title($name);
    if (empty($page)) {
        $page = get_page_by_path($name);
        if (empty($page)) {
            return null;
        }
    }
    return $page->ID;
}

include_once(TEMPLATE_PATH . '/lib/redux/framework.php');
include_once(TEMPLATE_PATH . '/lib/redux/config.php');

//define('ACF_LITE', true); //hiding ACF from admin panel after developement
include_once(TEMPLATE_PATH . '/lib/acf/acf.php');
include_once(TEMPLATE_PATH . '/lib/acf-repeater/acf-repeater.php');
include_once(TEMPLATE_PATH . '/lib/create-acf.php');
include_once(TEMPLATE_PATH . '/lib/acf-oembed/acf-oembed.php');


 


function add_categories() {
    register_taxonomy_for_object_type('post_tag', 'page');
    register_taxonomy_for_object_type('category', 'page');
}

add_action('init', 'add_categories');

/**
 * enabling fetured image in posts
 */
add_theme_support('post-thumbnails');


add_theme_support( 'custom-background' );


add_image_size( 'carousel-index', 900, 900 );
add_image_size( 'carousel-header', 300, 500 );
add_image_size( 'carousel-gallery', 900, 900 );
add_image_size( 'o-nama-page', 250, 188 );
add_image_size( 'konj-carousel', 90, 90 );
add_image_size( 'sponzors', 250, 250 );
add_image_size( 'index', 1024, 780 );



