<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js <?php echo ($GLOBALS['rwd'] == true)? 'rwd': ''; ?>"> <!--<![endif]-->

<head <?php language_attributes(); ?>>
    <meta charset="<?php echo bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <meta name="author" content="Udruga Mogu" />
    <meta name="contact" content="udruga.mogu@gmail.com" />
    <meta name="copyright" content="Copyright (c)2006-2014 Udruga Mogu. All Rights Reserved." />
    <meta name="description" content="<?php echo bloginfo( 'description' ); ?>" />
    <meta name="keywords" content="udruga, mogu, osijek, terapijsko, jahanje, konji, sport, invlaliditet, poseben potrebe" />
    <title><?php bloginfo('name'); ?><?php wp_title(); ?></title>
    <link rel="stylesheet" type="text/css" href="<?php echo TEMPLATE_URI; ?>/css/bootstrap.min.css" />
    
    <link rel ="stylsheet" type="text/css" href ="<?php echo TEMPLATE_URI; ?>/css/custom.css" />
   <link rel="stylesheet" type="text/css" href ="<?php echo TEMPLATE_URI; ?>/css/Main.css" />
    
    <link rel="sprite" type="image/png" href="<?php echo TEMPLATE_URI; ?>/sprites.png" />
    <script src="<?php echo TEMPLATE_URI; ?>/js/modernizr/modernizr-2.6.2.min.js"></script>
        <script src="<?php echo TEMPLATE_URI; ?>/js/modernizr/ietests.js"></script>
        <script type="text/javascript">
            
            Modernizr.load([
                {
                    test: Modernizr.ie9,
                    yep: '<?php echo TEMPLATE_URI; ?>/css/ie9.css'                    
                },
                {
                    test: Modernizr.ie8,
                    yep: [
                        '<?php echo TEMPLATE_URI; ?>/css/ie9.css',
                        '<?php echo TEMPLATE_URI; ?>/css/ie8.css'
                    ]                    
                },
                {
                    test: Modernizr.mq('only all'),
                    nope: '<?php echo TEMPLATE_URI; ?>/js/respond/respond.js'
                }
            ]);
        </script>
    
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?php echo TEMPLATE_URI; ?>/js/jquery/jquery-1.11.1.min.js"><\/script>');</script>
    <script src="<?php echo TEMPLATE_URI; ?>/js/bootstrap/bootstrap.min.js"></script>
    
    
    
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?> >
    <div class="container">
        

