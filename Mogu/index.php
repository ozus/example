<?php get_header( 'index'); ?>
<center><h2><b>Dobrodosli na stranicu Udruge Mogu</b></h2>
    <b><?php echo bloginfo( 'description'); ?></b></center>
<?php $id = get_page_id( 'Pocetna' ); ?>
<div class="container">
<div class="row">
    <?php $id = get_page_id( 'Elementi' ); ?>
    <div id ="horses" class ="carousel slide" data-ride ="carousel">
        <ol class= "carousel-indicators">
            <?php $counter = 0; ?>
            <?php if( have_rows( 'page_index_carousel', $id ) ) :
                    while( have_rows( 'page_index_carousel', $id ) ) :
                        the_row(); ?>
                        <?php if( $counter == 0 ) : ?>
                            <li data-target="#horses" data-slide-to="<?php echo $counter; ?>" class="active"></li>  
                        <?php endif; ?>
                        <?php if( $counter > 0 ) : ?>
                            <li data-target="#horses" data-slide-to="<?php echo $counter; ?>"></li>   
                        <?php endif; ?>
                        <?php $counter++; ?>    
                    <?php endwhile;
            endif; ?>
        </ol>
        <div class ="carousel-inner">
            <?php $counter1 = 0; ?>
            <?php if( have_rows( 'page_index_carousel', $id ) ) :
                while( have_rows( 'page_index_carousel', $id ) ) :
                    the_row(); ?>
                    <?php if( $counter1 == 0 ) : ?>
                        <div class="item active">
                    <?php endif; ?>
                    <?php if( $counter1 > 0 ) : ?>
                        <div class="item">
                    <?php endif; ?>
                     <?php $image = get_sub_field( 'page_index_carousel_image' ); ?>       
                     <img src="<?php echo $image['sizes']['index']; ?>" style="width:1024px;height:780px" alt="" /> 
                    <?php if( $counter1 == 0 ) : ?>
                        </div>
                    <?php endif; ?>
                    <?php if( $counter1 > 0 ) : ?> 
                        </div>
                    <?php endif; ?>
                    <?php $counter1++; ?>
                <?php endwhile;
            endif; ?>
        </div>
    </div>
</div>
 
</br>
<div class="curvedBorder">
<div class ="row">
    <div class ="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <center><b><h3>Uzivajte u posjetu nasoj stranici te nas obavezno posjetite</h3></b>
            <b><h3>Uđite na:</h3></b></center>
    </div>
</div>
</br>
<div class="row">
    <div class ="col-xs-6 col-sm-6 col-md-12 col-lg-12">
        <center>
            <?php $id = get_page_id( 'Elementi' ); ?>
                <?php if( get_field( 'page_index_links', $id ) ) :
                    if( in_array( 'Glavna stranica', get_field( 'page_index_links', $id ) ) ) : ?> 
                        <b><a href="<?php echo get_permalink( get_page_id( 'O Nama') ); ?>" class="LinkButton">Glavnu stranicu</a></b>
                <?php endif; ?>
                <?php if( in_array( 'Volonteri', get_field( 'page_index_links', $id ) ) ) : ?> 
                        <b><a href="<?php echo get_permalink( get_page_id( 'Volonteri') ); ?>" class="LinkButton">Stranicu Volontera</a></b>
                <?php endif; ?>
                <?php if( in_array( 'Aktualna dogadanja', get_field( 'page_index_links', $id ) ) ) : ?> 
                        <b><a href="<?php echo get_permalink( get_page_id( 'Aktualna Dogadanja') ); ?>" class="LinkButton">Aktualna Događanja</a></b>
                <?php endif; ?>
            <?php endif; ?>            
        </center>
    </div>
</div>
</br>
</br>
</div>
</br>
</br>
</br>
<div class="row">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-md-push-5 col-lg-push-5">
            <b><h3>Posvete Udruzi Mogu</h3></b>
        </div>
    </div>
    </br>
    <?php $id = get_page_id( 'Elementi' ); ?>
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-push-2 col-lg-push-2">
    <div id ="testimonials" class ="carousel slide" data-ride ="carousel">
        <div class ="carousel-inner">
            <?php $counter2 = 0; ?>
            <?php if( have_rows( 'page_index_repeater_testimonials', $id ) ) :
                while( have_rows( 'page_index_repeater_testimonials', $id ) ) :
                    the_row(); ?>
                    <?php if( $counter2 == 0 ) : ?>
                        <div class="item active">
                    <?php endif; ?>
                    <?php if( $counter2 > 0 ) : ?>
                        <div class="item">
                    <?php endif; ?>
                     <?php $image = get_sub_field( 'page_index_repeater_testimonials_img' ); ?>       
                     <img src="<?php echo $image['sizes']['index']; ?>" style="width:200px;height:200px" alt="" /> 
                     <div class="testimonials textBorder"><?php the_sub_field( 'page_index_repeater_testimonials_text' ); ?></div>
                    <?php if( $counter2 == 0 ) : ?>
                        </div>
                    <?php endif; ?>
                    <?php if( $counter2 > 0 ) : ?> 
                        </div>
                    <?php endif; ?>
                    <?php $counter2++; ?>
                <?php endwhile;
            endif; ?>
        </div>
            <a class="left carousel-control" href="#testimonials" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#testimonials" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
    </div>
    </div>    
</div>
</br>
</br>
</br>
</div>
<script>
$('#testimonials').carousel({interval: false});

$(document).on('mouseleave', '#testimonials', function() {

    $(this).carousel('pause');
});
</script>
<script>
$('.carousel').carousel({
    pause: "false"
});
</script>
<script>
$('.carousel').carousel({
  interval: 2000
})
</script>
