<?php

function set_pages() {
    global $pages;
    foreach ($pages as $page) {
        if (get_page_by_title($page['post_title']) == null) {
            if (isset($page['post_parent']) && !empty($page['post_parent'])) {
                $pageObject = get_page_by_title($page['post_parent']);
                $page['post_parent'] = $pageObject->ID;
            }
            wp_insert_post($page);
        }
    }
}

function unset_pages() {
    global $pages;
    foreach ($pages as $page) {
        $pageObject = get_page_by_title($page['post_title']);
        if ($pageObject != null) {
            wp_delete_post($pageObject->ID, true);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

function set_categories() {
    global $categories;
    foreach ($categories as $name) {
        if (!term_exists($name, 'category')) {
            wp_insert_term($name, 'category');
        }
    }
}

function unset_categories() {
    global $categories;
    foreach ($categories as $name) {
        $category = get_term_by('name', $name, 'category');
        if ($category != false) {
            wp_delete_term($category->term_id, 'category');
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

function set_tags() {
    global $tags;
    foreach ($tags as $name) {
        if (!term_exists($name, 'post_tag')) {
            wp_insert_term($name, 'post_tag');
        }
    }
}

function unset_tags() {
    global $tags;
    foreach ($tags as $name) {
        $tag = get_term_by('name', $name, 'post_tag');
        if ($tag != false) {
            wp_delete_term($tag->term_id, 'post_tag');
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

function set_front_page_and_blog_page() {
    global $page_on_front;
    global $page_for_posts;

    $frontPage = get_page_by_title($page_on_front);
    if ($frontPage != null) {
        update_option('page_on_front', $frontPage->ID);
        update_option('show_on_front', 'page');
    }

    $blog = get_page_by_title($page_for_posts);
    if ($blog != null) {
        update_option('page_for_posts', $blog->ID);
    }
}

function unset_front_page_and_blog_page() {
    update_option('page_on_front', null);
    update_option('show_on_front', null);

    update_option('page_for_posts', null);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

function set_permalinks() {
    update_option('permalink_structure', '/%postname%/');
    update_option('category_base', null);
    update_option('tag_base', null);
}

function unset_permalinks() {
    update_option('permalink_structure', null);
    update_option('category_base', null);
    update_option('tag_base', null);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

if (!get_option('theme_installed')) {
    set_pages();
    set_categories();
    set_tags();
    set_front_page_and_blog_page();
    set_permalinks();

    update_option('theme_installed', 1);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
add_action('admin_post_force_theme_reset', 'force_theme_reset');

function force_theme_reset() {
    unset_pages();
    set_pages();

    unset_categories();
    set_categories();

    unset_tags();
    set_tags();

    unset_front_page_and_blog_page();
    set_front_page_and_blog_page();

    unset_permalinks();
    set_permalinks();

    update_option('theme_installed', 1);

    wp_redirect(admin_url('edit.php?post_type=page'));
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
add_action('admin_post_force_theme_unset', 'force_theme_unset');

function force_theme_unset() {
    unset_pages();
    unset_categories();
    unset_tags();
    unset_front_page_and_blog_page();
    unset_permalinks();

    wp_redirect(admin_url('edit.php?post_type=page'));
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
add_action('admin_post_force_theme_unset_all', 'force_theme_unset_all');

function force_theme_unset_all() {
    $types = array('post', 'page', 'acf');
    foreach ($types as $type) {
        $args = array(
            'post_type' => $type,
            'posts_per_page' => -1,
            'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
        );
        $posts = new WP_Query($args);
        if (!empty($posts)) {
            foreach ($posts as $post) {
                wp_delete_post($post->ID, true);
            }
        }
    }
    wp_redirect(admin_url('edit.php?post_type=page'));
}
