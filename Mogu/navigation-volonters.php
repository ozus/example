<div class="row">
    <nav class="navbar navbar-default" role="navigation">
	<div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
		      <span class="sr-only">Toggle navigation</span>
		      <span class="icon-bar"></span>
		      <span class="icon-bar"></span>
		      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
		</button>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="colapse">
		<ul class="nav navbar-nav">
		    <li><a href ="<?php echo get_permalink( get_page_id( 'Volonteri') ); ?>" >Pocetna</a></li>
		    <li class="dropdown"><a href="#" data-toggle ="dropdown">Slike<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href ="<?php echo get_permalink( get_page_id( 'Dodaj Sliku' ) ); ?>" >Dodaj Sliku</a></li>
                            <li><a href ="<?php echo get_permalink( get_page_id( 'Gallery' ) ); ?>">Ocijeni Sliku</a></li>
                            <li><a href ="<?php echo get_permalink( get_page_id( 'Gledanje Galerije' ) ); ?>">Galerija</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"><a href="#" data-toggle ="dropdown">Video<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href ="<?php echo get_permalink( get_page_id( 'Dodaj Video' ) ); ?>" >Dodaj Video</a></li>
                            <li><a href ="<?php echo get_permalink( get_page_id( 'Gledanje Videa' ) ); ?>" >Gledanje Videa</a></li>
                        </ul>
                    </li> 
                    <li><a href ="<?php echo get_permalink( get_page_id( 'Obavijesti') ); ?>" >Obavijesti</a></li>
                    <li><a href ="<?php echo get_permalink( get_page_id( 'Chat' ) ); ?>" >Chat</a></li> 
                </ul>   
	    </div><!-- end navbar-collapse -->
        </div><!-- end container-fluid -->
    </nav>
</div>

