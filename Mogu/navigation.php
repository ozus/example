<div class="row">
    <nav class="navbar navbar-default navigationBar" role="navigation">
	<div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
		      <span class="sr-only">Toggle navigation</span>
		      <span class="icon-bar"></span>
		      <span class="icon-bar"></span>
		      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
		</button>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="colapse">
		<ul class="nav navbar-nav">
		    <li class="dropdown navButton"><a style="color:#ffffff;" href="#" data-toggle ="dropdown">O Nama<span class="caret"></span></a>
                        <ul class ="dropdown-menu navigationBar">
                            <li class="navButton"><a style="color:#ffffff;" href ="<?php echo get_permalink( get_page_id( 'O Nama') ); ?>" >O Nama</a></li>
                            <li class="navButton"><a style="color:#ffffff;" href ="<?php echo get_permalink( get_page_id( 'O Nasim Konjima') ); ?>" >O Nasim Konjima</a></li>
                            <li class="navButton"><a style="color:#ffffff;" href ="<?php echo get_permalink( get_page_id( 'Sto Cinimo?') ); ?>" >Sto Cinimo</a></li>
                            <li class="navButton"><a style="color:#ffffff;" href ="<?php echo get_permalink( get_page_id( 'Komu Sluzimo') ); ?>" >Komu Sluzimo</a></li>
                        </ul>
                    </li>
		    <li class="navButton"><a style="color:#ffffff;" href ="<?php echo get_permalink( get_page_id( 'Terapijsko Jahanje') ); ?>" >Terapijsko Jahanje</a></li>
		    <li class="navButton"><a style="color:#ffffff;" href ="<?php echo get_permalink( get_page_id( 'Hipoterapija') ); ?>" >Hipoterapija</a></li>
                    <li class="navButton"><a style="color:#ffffff;" href ="<?php echo get_permalink( get_page_id( 'Programi') ); ?>" >Programi</a></li>
                    <li class="navButton"><a style="color:#ffffff;" href ="<?php echo get_permalink( get_page_id( 'Sponzori') ); ?>" >Sponzori</a></li>
                    <li class="navButton"><a style="color:#ffffff;" href ="<?php echo get_permalink( get_page_id( 'Vide' ) ); ?>" >Video</a></li>
                    <li class="navButton"><a style="color:#ffffff;" href ="<?php echo get_permalink( get_page_id( 'Aktualna Izvjesca') ); ?>" >Aktualna Izvjesca</a></li>
                    <li class="navButton"><a style="color:#ffffff;" href ="<?php echo get_permalink( get_page_id( 'Sport') ); ?>" >Sport</a></li>
                    <li class="dropdown navButton"><a style="color:#ffffff;" href="#" data-toggle ="dropdown">Obrasci<span class="caret"></span></a>
                        <ul class="dropdown-menu navigationBar">
                            <li class="navButton"><a style="color:#ffffff;" href ="<?php echo TEMPLATE_URI; ?>/form12.html" target="blank" >Obrazac za prijavu</a></li>
                            <li class="navButton"><a style="color:#ffffff;" href ="<?php echo TEMPLATE_URI; ?>/obrazac_dolazakk.php" target="blank" >Dolazak na terapiju</a></li>
                        </ul>
                    </li>    
	    </div><!-- end navbar-collapse -->
        </div><!-- end container-fluid -->
    </nav>
</div>

