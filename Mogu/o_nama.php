<?php
/*
 * Template Name: Predlozak O Nama
 */
?>
<?php get_header(); ?>
<div class ="container">
    <div class="curvedBorder">
    <div calss ="row">
        <div class ="col-md-12 col-lg-12 col-sm-12">
            <h2><b><div style ="color:<?php the_field( 'page_o_nama_title_color'); ?>" ><?php the_field( 'page_o_nama_title' ); ?></div></b></h2>
        </div>
    </div>
    <div class ="row">
        <div class ="col-md-4 col-lg-4 col-sm-6" >
            <?php 
                if( have_rows( 'page_o_nama_images' ) ) :
                    while( have_rows( 'page_o_nama_images') ) :
                        the_row(); ?>
                        <?php $image = get_sub_field( 'page_o_nama_repeater_image' ); ?>
            <center><img src ="<?php echo $image['sizes']['o-nama-page']; ?>" alt="O nama " /></center>
                    <?php endwhile;
                endif; ?>
        </div>
        <div class ="col-md-8 col-lg-8 col-sm-6">
            <?php if( get_field( 'page_o_nama_frame_check') ) :
                if( in_array( 'Sa okvirom', get_field( 'page_o_nama_frame_check') ) ) : ?>
                    <table cellpadding="50" align="center" style ="border:5px solid black"><tr><td align="center">
                <?php endif;
            endif; ?>
            <?php the_field( 'page_o_nama_text' ); ?>
            <?php if( get_field( 'page_o_nama_frame_check') ) :
                if( in_array( 'Sa okvirom', get_field( 'page_o_nama_frame_check') ) ) : ?>
                    </td></tr></table>
                <?php endif;
            endif; ?>
        </div>
    </div>
    </br>
    <div class ="row">
        <div class ="col-xs-6 col-sm-4 col-md-4 col-lg-4">
            <h3><b>Gdje se nalazimo?</b></h3>
            </br>
            <?php the_field( 'page_o_nama_adress' ); ?>
        </div>
        <div class ="col-xs-6 col-sm-4 col-md-4 col-lg-4">
            <a href="#" data-toggle="modal" data-target="#myModal"><h3><b>Kako nas kontaktirati?</b></h3></a>
            </br>
            <?php
                if( have_rows( 'page_o_nama_repeater_contacts' ) ) :
                    while( have_rows( 'page_o_nama_repeater_contacts' ) ) :
                        the_row(); ?>
                        <?php the_sub_field( 'page_o_nama_repeater_contacts_type'); ?><?php the_sub_field( 'page_o_nama_repeater_contacts_number' ); ?>
                        </br>
                    <?php endwhile;
                endif;
            ?>    
        </div>
        <div class ="col-xs-6 col-sm-4 col-md-4 col-lg-4">
            <h3><b>Kako donirati?</b></h3>
            </br>
            <?php
                if( have_rows( 'page_o_nama_repeater_accounts' ) ) :
                    while( have_rows( 'page_o_nama_repeater_accounts') ) :
                        the_row(); ?>
                        <?php the_sub_field( 'page_o_nama_repeater_accounts_acc' ); ?>
                    <?php endwhile;
                endif;
            ?>    
        </div>
    </div>
    </br>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Imate li pitanje ili poruku za nas?</h4>
      </div>
      <div class="modal-body">
          <form action="<?php echo TEMPLATE_URI; ?>/mail.php" method="post">
              <label for="email">Vaša e-mail adresa</label></br>
              <input type="email" name="email" id="email"></br></br>
              <label for="message">Vaše pitanje/poruka</label></br>
              <textarea rows="5" cols="60" name="message" id="message"></textarea></br>
              <input type="submit" value="Pošalji">
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</br>
<?php get_footer(); ?>


