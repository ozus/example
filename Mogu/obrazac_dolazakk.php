
<html>
    <head>
        <title>
            Dolazak na terapiju
        </title>
        <meta charset="UTF-8">
    </head>
    <body>
        <b><h2>Udruga Mogu - dolazak na terapiju</h2></b>
        <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="get">
            <table cellpadding="10">
                <tr>
                    <td>
                        <b>Ime korisnika:</b> <input type="text" name="name">
                    </td>
                    <td>
                         <b>Prezime korisnika:</b> <input type="text" name="Lname">
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom:1px solid black">
                        <b>Odaberite mjesec:</b> <select name="month">
                            <option value="sjecanj">Sjećanj</option>
                            <option value="veljaca">Veljaća</option>
                            <option value="ozujak">Ožujak</option>
                            <option value="travanj">Travanj</option>
                            <option value="svibanj">Svibanj</option>
                            <option value="lipanj">Lipanj</option>
                            <option value="srpanj">Srpanj</option>
                            <option value="kolovoz">Kolovoz</option>
                            <option value="rujan">Rujan</option>
                            <option value="listopad">Listopad</option>
                            <option value="studeni">Studeni</option>
                            <option value="prosinac">Prosinac</option>
                        </select>
                    </td>
                    <td style="border-bottom:1px solid black">
                        <b>Odaberite datum:</b><select name="date">
                                <option value="1.">1.</option>
                                <option value="2.">2.</option>
                                <option value="3.">3.</option>
                                <option value="4.">4.</option>
                                <option value="5.">5.</option>
                                <option value="6.">6.</option>
                                <option value="7.">7.</option>
                                <option value="8.">8.</option>
                                <option value="9.">9.</option>
                                <option value="10.">10.</option>
                                <option value="11.">11.</option>
                                <option value="12.">12.</option>
                                <option value="13.">13.</option>
                                <option value="14.">14.</option>
                                <option value="15.">15.</option>
                                <option value="16.">16.</option>
                                <option value="17.">17.</option>
                                <option value="18.">18.</option>
                                <option value="19.">19.</option>
                                <option value="20.">20.</option>
                                <option value="21.">21.</option>
                                <option value="22.">22.</option>
                                <option value="23.">23.</option>
                                <option value="24.">24.</option>
                                <option value="25.">25.</option>
                                <option value="26.">26.</option>
                                <option value="27.">27.</option>
                                <option value="28.">28.</option>
                                <option value="29.">29.</option>
                                <option value="30.">30.</option>
                                <option value="31.">31.</option>
                        </select>
                    </td>
                    <td style="border-bottom:1px solid black">
                        <b>Dio dana:</b><input type="checkbox" name="arivels[]" value="prijepodne"> Prijepodne <input type="checkbox" name="arivels[]" value="poslijepodne">Poslijepodne
                    </td>
                </tr>
                <tr>
                    <td>
                        
                    </td>
                    <td>
                        
                    </td>
                    <td>
                        <input type="submit" value="Pošalji">
                        <input type="button" value="Poništi" name="clear" onclick="clearForm(this.form);">
                    </td>
                </tr>
            </table>    
        </form>
        <?php error_reporting(0); ?>
        
         <?php
         $month = $_GET['month'];
        $date = $_GET['date'];
        $arivels = $_GET['arivels'];
        $name = $_GET['name'];
        $Lname = $_GET['Lname']; 
        
        if( $month && $date && $name && $arivels && $Lname ) :
            $temporaly = "temporaly.doc";
                if( $temp = fopen( $temporaly, 'a' ) ) :
                    $line2 = "Dan:" .$date ;
                    fputs( $temp, $line2 );
                    $line1 = "$month\n";
                    fputs( $temp, $line1 );
                    $line3 = "Ime:$name\n";
                    fputs( $temp, $line3 );
                    $line4 = "Prezime:$Lname\n";
                    fputs( $temp, $line4 );
                    foreach( $arivels as $arivel ) :
                        $line5 = "Dolazak:$arivel\n";
                        fputs( $temp, $line5 );
                    endforeach;
                    $line6 = "Datum prijave:" .date('d.m.Y H:i') . "\n";
                    fputs( $temp, $line6 );
                    $line7 = "/////////////////////////////////////////////////////////\n";
                    fputs( $temp, $line7 );
                    fclose( $temp );
                endif;
        endif;
        if(file_exists( $temporaly) ) :
            if( $temp1 = fopen( $temporaly, 'r' ) ) :
                $text = '';
                while( !feof( $temp1 ) ) :
                $text = fgets( $temp1 ) . $text;
                endwhile;
            fclose( $temp1 );    
            endif;
        endif;
        if( !$month || !$date || !$name || !$arivels || !$Lname ) :
            echo "Kako bi mogli nastaviti molimo vas da upišete potrebne podatke";
        endif; 
        if( $text) : ?>
        <h3>Odabrali ste:</h3>
        <table border="2">
            <tr>
                <td>
                    Ime:<?php echo $name; ?>
                </td>
                <td>
                    Prezime:<?php echo $Lname; ?>
                </td>
            </tr>
            <tr>
                <td>
                    Datum dolaska: <?php echo $date; ?><?php echo $month; ?>
                </td>
                <td>
                    Vrijeme dolaska: <?php foreach( $arivels as $arivel ) : if( $arivel ) : echo $arivel . "||"; endif; endforeach; ?>
                </td>
            </tr>
        </table>
        
        <table border="0">
            <tr>
                <td>
                    <form action="dolazak.php" method="get">
                        Želite li poslati ove podatke?<select name="data">
                            <option value="confirme">Potvrdi</option>
                            <option value="discard">Odbaci</option>
                        </select>
                        <input type="submit" value="Odaberi"> 
                    </form>
                    Ukoliko odaberete potvrdi podatci će biti zabilježeni, ukoliko odaberete odbaci podatci če biti poništeni.
                </td>
            </tr>
        </table>
        <?php endif; ?>
    <script>
        function clearForm(oForm) {
    
  var elements = oForm.elements; 
    
  oForm.reset();

  for(i=0; i<elements.length; i++) {
      
  field_type = elements[i].type.toLowerCase();
  
  switch(field_type) {
  
    case "text": 
    case "password": 
    case "textarea":
          case "hidden":   
      
      elements[i].value = ""; 
      break;
        
    case "radio":
    case "checkbox":
        if (elements[i].checked) {
          elements[i].checked = false; 
      }
      break;

    case "select-one":
    case "select-multi":
                elements[i].selectedIndex = -1;
      break;

    default: 
      break;
  }
    }
}
    </script>    
    </body>
</html>
