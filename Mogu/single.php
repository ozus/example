<?php if( in_category( 'izvjesca' ) || in_category( 'obavijesti' ) ) : ?>
<?php get_header(); ?>
<?php endif; ?>
<?php if( in_category( 'volonteri' ) ) : ?>
<?php get_header( 'volonters' ); ?>
<?php endif; ?>
<?php while( have_posts() ) : the_post(); ?>
            <div class ="container">
                <center><div class="row">
                    <div class ="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <table border="0" bgcolor="#E0E0E0" style="width:500px"><tr><td bgcolor="#E0E0E0">
                        <h3><b><div style="color:#000000; text-decoration:none"><a href ="<?php get_permalink(); ?>" target="blank"><?php the_title(); ?></a></div></b></h3>
                        </td></tr></table>
                    </div>
                </div>
                <div class ="row">
                    <table border="0" bgcolor="#000000" style="border-bottom:3px black solid; width:500px;">
                        <tr>
                            <td align="left" bgcolor="#E0E0E0" style="padding:5px">
                                 <?php echo get_the_date(); ?>
                            </td>
                            <td align="left" bgcolor="#E0E0E0" style="padding:5px">
                                 <?php the_author(); ?>
                            </td>
                            <td align="right" bgcolor="#E0E0E0" style="padding:5px">
                                 <?php comments_popup_link( 'nema komentara', '1 komentar', 'pogledaj komentare' ); ?>
                            </td>
                        </tr>
                    </table>    
                </div>
                <div class ="row">
                    <table border="0" style="width:500px" bgcolor="#000000">
                        <tr>
                            <td align="left" bgcolor="#E0E0E0" style="padding:10px">
                                <?php the_content(); ?>
                            </td>
                        </tr>
                    </table>
                </div></center> 
                
                <center><?php comments_template(); ?></center>
            </div>    
<?php endwhile; ?>
<?php if( in_category( 'izvjesca' ) || in_category( 'obavijesti' ) ) : ?>
<?php get_footer(); ?>
<?php else : ?>
<?php get_footer( 'volonters' ); ?>
<?php endif; ?>

