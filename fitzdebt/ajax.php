<?php

add_action('wp_ajax_send_mail', 'send_mail');
add_action('wp_ajax_nopriv_send_mail', 'send_mail');

function send_mail() {
    //sanitizing
    $sanitize_conditions = array(
        'fitzdebt_name' => array('filter' => FILTER_SANITIZE_STRING),
        'fitzdebt_company' => array('filter' => FILTER_SANITIZE_STRING),
        'fitzdebt_email' => array('filter' => FILTER_SANITIZE_EMAIL),
        'fitzdebt_phone' => array('filter' => FILTER_SANITIZE_NUMBER_INT),
        'fitzdebt_message' => array('filter' => FILTER_SANITIZE_STRING)
    );
    global $data;
    $data = filter_input_array(INPUT_POST, $sanitize_conditions);

    //returning quotes
    $data['fitzdebt_message'] = htmlspecialchars_decode($data['fitzdebt_message'], ENT_QUOTES);

    //validating
    $validate_conditions = array(
        'fitzdebt_email' => array('filter' => FILTER_VALIDATE_EMAIL)
    );
    $data = array_merge($data, filter_var_array($data, $validate_conditions));

    //list or required variables
    $required = array(
        'fitzdebt_name' => 'Name',
        'fitzdebt_email' => 'Email',
        'fitzdebt_message' => 'Message'
    );

    //checking if something is missing
    $message = '';
    foreach ($required as $name => $value) {
        if ($data[$name] == '' || $data[$name] == false) {
            $message.=$value . ' is required. ';
        }
    }
    if ($message) {
        header('Content-Type: application/json');
        echo json_encode(array('message' => $message, 'error' => true));
        die();
    }

    //initalizing phpmailer
    require TEMPLATEPATH . '/phpmailer/PHPMailerAutoload.php';
    $mail = new PHPMailer;
    $mail->FromName = 'Fitzgerald Debt Acquisitions, LLC Web Form';
    $mail->Subject = 'Fitzgerald Debt Acquisitions, LLC Web Form';

    //adding addresses
    $mail->addAddress('jhartman@fitzdebt.com', 'Jeff Hartman');
    //$mail->addAddress('zvidakov@gmail.com', 'Zoran');
    //$mail->addAddress('bruno@pavelja.com', 'dev');
    //building message
    $mail->isHTML(true);
    //adding embedded image, so don't forget to configure email server!
    //X-Priority: 3 -> who cares
    //X-Mailer: PHPMailer 5.2.7 (https://github.com/PHPMailer/PHPMailer/) -> who cares
    //MIME-Version: 1.0 -> IMPORTANT
    //Content-Type: multipart/related; -> IMPORTANT
    //boundary="b1_3e0a0fc08eecccac498a7d82b0a23e93" -> random string for setting boundary
    //Content-Transfer-Encoding: 8bit -> could be 16, but not important at the moment
    //cid works in buffering fine, no globals needed
    $mail->AddEmbeddedImage(TEMPLATEPATH . "/images/fitz.png", "logo", "fitz.png");

    //checking for template
    if (is_file(TEMPLATEPATH . '/contact-form-template.php')) {
        //if template exists, load it into body
        ob_start();
        include TEMPLATEPATH . '/contact-form-template.php';
        $mail->Body = ob_get_clean();
    } else {
        //if template doesn't exist, output message
        header('Content-Type: application/json');
        echo json_encode(array('message' => 'Contact form template is missing.', 'error' => true));
        die();
    }

    //sending mail
    if ($mail->send()) {
        header('Content-Type: application/json');
        echo json_encode(array('message' => '<strong>Thanks for contacting us, we will reply to you as soon as possible.</strong>', 'error' => false));
        die();
    } else {
        header('Content-Type: application/json');
        echo json_encode(array('message' => 'Something went wrong!', 'error' => true));
        die();
    }
    die();
}
