<?php
/*
 * Template Name: Consumer Assets
 */
?>

<?php $id = get_page_id( 'Services' ); ?>



 <div class="item">
                        <div id="service-consumerassets" class="service-page">
                            <h2><?php the_field( 'services_consumer_title', $id ); ?></h2>
                            <p><?php the_field( 'services_consumer_text', $id ); ?></p>
                            <div class="serviceBoxPage">
                                
                                <?php if( have_rows( 'services_consumer_repeater', $id ) ) :
                                        while( have_rows( 'services_consumer_repeater', $id ) ):
                                             the_row(); ?>
                                
                                             <div class="col-md-6 col-xs-12">
                                                 <div class="contentServiceBox">
                                                     <h3 class="purple"><?php the_sub_field( 'services_consumer_repeater_name' ); ?></h3>
                                                     <p><?php the_sub_field( 'services_consumer_repeater_desc' ); ?></p>
                                                 </div>
                                             </div>    
                                        <?php endwhile; ?>
                                <?php endif; ?>
                                
                                
                            </div>
                            <p><?php the_field( 'services_consumer_bottom_message', $id ); ?></p>
                            <div class="row shadowDivider"></div>
                            <div class="services-back-box">
                                <button type="button" class="button services-back-button" data-target="#services-slider" data-slide-to="0">Back</button>
                            </div>
                        </div>
                    </div>

