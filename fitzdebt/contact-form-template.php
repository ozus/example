<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Fitzgerald Debt Acquisitions LLC</title>
    </head>
    <body style="margin: 0; padding: 0;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%"> 
            <tr>
                <td style="padding: 10px 0 30px 0;">
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" style="border: 1px solid #cccccc; border-collapse: collapse;">
                        <tr>
                            <td bgcolor="#ffffff" >
                                <table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="100%"  style="border-bottom: 1px solid #cccccc;">
                                    <tr>
                                        <td width="300" align="center" style=" color: #666666; font-family: Arial, sans-serif; font-weight:bold; font-size:18px; padding:20px 0px 20px 0px">
                                            <img src="cid:logo" height="63" width="250"><br />
                                        </td>
                                        <td bgcolor="#ffffff" >
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td width="120" align="left" style="padding:14px 0px 14px 0px; color: #282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold;">
                                                        Name:
                                                    </td>
                                                    <td width="380" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px;">
                                                        <?php echo $data['fitzdebt_name']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="120" align="left" style="padding:14px 0px 14px 0px; color: #282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold;">
                                                        E-Mail:
                                                    </td>
                                                    <td width="380" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px;">
                                                        <a href="mailto:<?php echo $data['fitzdebt_email']; ?>" style="color:#876b9e; font-weight:bold;"><?php echo $data['fitzdebt_email']; ?></a>
                                                    </td>
                                                </tr>
                                                <tr> 
                                                    <td width="120" align="left" style="padding:14px 0px 14px 0px; color: #282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold;">
                                                        Company:
                                                    </td>
                                                    <td width="380" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px;">
                                                        <?php echo $data['fitzdebt_company']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="180" align="left" style="padding:14px 0px 14px 0px; color:#282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold;">
                                                        Phone number:
                                                    </td>
                                                    <td width="380" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px;">
                                                        <?php echo $data['fitzdebt_phone']; ?>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                    </tr>
                                </table>
                                <tr>
                                    <td>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="center" style="color:#383838;padding:25px 20px 85px 20px;font-family: Arial, sans-serif; font-size: 14px;">
                                                    <p><?php echo $data['fitzdebt_message']; ?></p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td bgcolor="#313131">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="center" style="padding:11px 0px 9px 0px; font-size:9px; font-family:Arial, sans-serif; color:#cccccc;">
                                                    Copyright <strong style="color:#876b9e; font-weight:bold;">Fitzgerald Debt Acquisitions LLC&#8482;&nbsp;&copy; </strong>, <?php echo date('Y'); ?>.
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                </table>
                            </td>
                        </tr> 
                    </table>
                    </body>
                    </html>