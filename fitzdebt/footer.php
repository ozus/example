<div id="footer" class="footerSection">
    <div class="container">
        <div class="row">
            <div class="footer-col logos">
                <img src="<?php echo TEMPLATE_URI; ?>/images/page/pci.png" alt="">
                <a href="//www.dbainternational.org/" title="DBA International" target="_blank"><img src="<?php echo TEMPLATE_URI; ?>/images/page/dba.png" alt=""></a>
                <a href="//www.acainternational.org/" title="ACA International" target="_blank"><img src="<?php echo TEMPLATE_URI; ?>/images/page/aca.png" alt=""></a>
            </div>
            <div class="footer-col copyright"><span class="purple">Fitzgerald Debt Acquisitions LLC&trade;</span>&nbsp;&copy; <?php echo date('Y'); ?> | Privacy Policy</div>
            <div class="footer_right">
                <div class="footer-col social">
                    <a href="//www.facebook.com/DebtSales.DebtPortfolios.DebtBuyers.DebtBrokers?__req=8" title="" target="_blank"><span class="sprite facebook"></span></a>
                    <a href="//twitter.com/FitzDebtSales" title="" target="_blank"><span class="sprite twitter"></span></a>
                    <!--<a href="#" title="" target="_blank"><span class="sprite rss"></span></a>-->
                </div>
                <div class="footer-col odinWEB">WEB by <a href="//www.odindesign.com/" title="OdinDesign Design" target="_blank"><span class="sprite odin"></span></a></div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo TEMPLATE_URI; ?>/js/main.js"></script>
<?php wp_footer(); ?>
</body>
</html>
