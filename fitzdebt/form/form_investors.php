<?php

/**
 * Template Name: Investors Form
 */
?>

<?php acf_form_head(); ?>
<?php get_header(); ?>

<?php get_template_part( 'form/form_navigation'); ?>

     <?php while ( have_posts() ) : the_post(); ?>

                <?php acf_form(array(
					'post_id'	=> get_page_id( 'Investors' ),
					'post_title'	=> false,
					'submit_value'	=> 'Insert data for Investors page!'
	        )); ?>
     <?php endwhile; ?>
</br>

<?php get_template_part( 'form/form_navigation' ); ?>

<?php get_footer(); ?>

