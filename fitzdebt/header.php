<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js <?php echo ($GLOBALS['rwd'] == true)? 'rwd': ''; ?>"> <!--<![endif]-->
    <head <?php language_attributes(); ?>>
        <meta charset="<?php echo bloginfo('charset'); ?>" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
        <meta name="author" content="Odin d.o.o." />
        <title><?php bloginfo('name'); ?> | <?php is_home() ? bloginfo('description') : wp_title(''); ?></title>
        <link rel="icon" type="image/ico" href="<?php echo TEMPLATE_URI; ?>/favicon.ico" />
        <link rel="icon" type="image/x-icon" href="<?php echo TEMPLATE_URI; ?>/favicon.ico" />
        <link rel="icon" type="image/png" href="<?php echo TEMPLATE_URI; ?>/favicon.png" />
        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo TEMPLATE_URI; ?>/apple-touch-icon.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo TEMPLATE_URI; ?>/apple-touch-icon-72x72.png" /> 
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo TEMPLATE_URI; ?>/apple-touch-icon-114x114.png" />
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo TEMPLATE_URI; ?>/apple-touch-icon-144x144.png" />
        <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css' />
        <meta property="og:image" content="http://fitzdebt.com/wp-content/themes/fitz/300x300.png" />
        <meta name="twitter:image" content="http://fitzdebt.com/wp-content/themes/fitz/300x300.png">
        <meta itemprop="image" content="http://fitzdebt.com/wp-content/themes/fitz/300x300.png"> 
        <link rel="stylesheet" type="text/css" href="<?php echo TEMPLATE_URI; ?>/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo TEMPLATE_URI; ?>/css/owl.carousel.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo TEMPLATE_URI; ?>/css/pickadate-default.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo TEMPLATE_URI; ?>/css/pickadate-default.date.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo TEMPLATE_URI; ?>/css/fitzgeraldfont.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo TEMPLATE_URI; ?>/css/sprites.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo TEMPLATE_URI; ?>/css/main.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo TEMPLATE_URI; ?>/css/main-responsive.css" />
        <script src="<?php echo TEMPLATE_URI; ?>/js/modernizr/modernizr-2.6.2.min.js"></script>
        <script src="<?php echo TEMPLATE_URI; ?>/js/modernizr/ietests.js"></script>
        <script type="text/javascript">
            
            Modernizr.load([
                {
                    test: Modernizr.ie9,
                    yep: '<?php echo TEMPLATE_URI; ?>/css/ie9.css'                    
                },
                {
                    test: Modernizr.ie8,
                    yep: [
                        '<?php echo TEMPLATE_URI; ?>/css/ie9.css',
                        '<?php echo TEMPLATE_URI; ?>/css/ie8.css'
                    ]                    
                },
                {
                    test: Modernizr.mq('only all'),
                    nope: '<?php echo TEMPLATE_URI; ?>/js/respond/respond.js'
                }
            ]);
        </script>
        
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="<?php echo TEMPLATE_URI; ?>/js/jquery/jquery-1.10.2.min.js"><\/script>');</script>
        <script src="<?php echo TEMPLATE_URI; ?>/js/bootstrap/bootstrap.min.js"></script>
        <script src="<?php echo TEMPLATE_URI; ?>/js/owl-carousel/owl.carousel.min.js"></script>
        <script src="<?php echo TEMPLATE_URI; ?>/js/pickadate/picker.js"></script>
        <script src="<?php echo TEMPLATE_URI; ?>/js/pickadate/picker.date.js"></script>
        <script src="<?php echo TEMPLATE_URI; ?>/js/pickadate/legacy.js"></script>
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
        <!--[if lt IE 9]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>