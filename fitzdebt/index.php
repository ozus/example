<?php

get_header();
get_template_part('part', 'navigation');
get_template_part('page', 'home');
get_template_part('page', 'about');
get_template_part('page', 'investors');
get_template_part('page', 'services');
get_template_part('page', 'contact');
get_footer();
?>