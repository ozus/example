
$(document).ready(function() {
    
    $('body').scrollspy({
        target: '#navigation',
        offset: 100
    });
    
    var owl = $('#testimonials-slider');
    owl.owlCarousel({
        items: 1,
        singleItem: true,
        stopOnHover: true,
        pagination: false,
        autoHeight: true
    });
    
    $(document).on('click', '.testimonials-control', function() {
        var $t = $(this);
        if($t.hasClass('left')) {
            owl.trigger('owl.prev');
        }
        else if($t.hasClass('right')) {
            owl.trigger('owl.next');
        }
    });
    
    $('#services-slider').carousel({
        wrap: false,
        interval: false
    });
    $('#fitz_modalForm_BalloonDate').pickadate({
        selectYears: true,
        selectMonths: true
    })
    $('#fitz_modalForm_dateFirstNote').pickadate({
        selectYears: true,
        selectMonths: true
    })
    $('#fitz_modalForm_AppraisalDate').pickadate({
        selectYears: true,
        selectMonths: true
    })
    $('#input_date').pickadate({
        selectYears: true,
        selectMonths: true
    })
    $('#fitz_modalForm_propertyDescrition').prop('selectedIndex', -1)
    $('#fitz_modalForm_creditPayor').prop('selectedIndex', -1)
    $('#fitz_modalForm_sell').prop('selectedIndex', -1)
});

$(window).on('scroll', function() {
    $window = $(this);
    var scroll = $window.scrollTop();
    if($window.width() >= 768 && !$('html').hasClass('rwd'))
    {
        $('.parallax').each(function() {
            var offset = $(this).parent('.parallax-container').offset().top - $window.innerHeight() - scroll;
            if(scroll >= offset)
            {
                var y = offset / 3.5;
                $(this).css('-webkit-transform', 'translate3d(0px, ' + y + 'px , 0px)');
                $(this).css('-moz-transform',"translate3d(0px, " + y + "px, 0px)");
                $(this).css('transform', 'translate3d(0px, ' + y + 'px , 0px)');
            }
        });
    }
    else {
        $('.parallax').each(function() {
            $(this).attr('style', '');
        });
    }
    if(scroll > $('#home').outerHeight())
        $('#navigation').addClass('not-as-high');
    else
        $('#navigation').removeClass('not-as-high');
    
    
});

$(window).on('resize', function() {
    window.setInterval(function() {
        $('body').scrollspy('refresh');
    }, 100);
});

$(document).on('click', '#nav li a', function() {
    var target = $(this).attr('data-target');
    var $target = $(target);
    var navHeight = (target === '#home')? 76 : 50;
    $('html, body').animate({scrollTop: $target.offset().top - navHeight}, 400);
});

/*******************************************************************************
 *  About Us more info 
 *******************************************************************************/
$(document).on('click', '#about-us-more-info-button', function() {
    var $cont = $('#about-us-more-info-container');
    var $inner = $cont.children('.more-info-inner');
    var owl = $('#testimonials-slider').data('owlCarousel');
    if($cont.hasClass('open'))
    {
        owl.stop();
        $(this).removeClass('pressed');
        $cont.css('height', $cont.outerHeight() + 'px');
        $cont.css('height'); /* This line must be here!! */        
        $cont.removeClass('no-transition');
        $cont.css('height', '0px');
        
        window.setTimeout(function() {
            $cont.removeClass('open');
            $cont.attr('style', '');
        }, 400);
    }
    else
    {
        $(this).addClass('pressed');
        $cont.css('height', ($inner.outerHeight() + 40) + 'px');
        owl.play(1000);
        window.setTimeout(function() {
            $cont.addClass('no-transition');
            $cont.addClass('open');         
            $cont.attr('style', '');
        }, 400);
    }
});

$(document).on('click', '#about-us-more-info-close-button', function() {
    var navHeight = $('#navigation').outerHeight();
    $('html, body').animate({scrollTop: $('#about-us').offset().top - navHeight}, 400);
    $('#about-us-more-info-button').trigger('click');
});


/*******************************************************************************
 *  Investors more info 
 *******************************************************************************/
$(document).on('click', '#investors-more-info-button', function() {
    var $cont = $('#investors-more-info-container');
    var $inner = $cont.children('.more-info-inner');
    if($cont.hasClass('open'))
    {
        $(this).removeClass('pressed');
        $cont.css('height', $cont.outerHeight() + 'px');
        $cont.css('height'); /* This line must be here!! */     
        $cont.removeClass('no-transition');
        $cont.css('height', '0px');
        window.setTimeout(function() {
            $cont.removeClass('open');
            $cont.attr('style', '');
        }, 400);
    }
    else
    {
        $(this).addClass('pressed');
        $cont.css('height', ($inner.outerHeight() + 15) + 'px');
        window.setTimeout(function() {
            $cont.addClass('no-transition');
            $cont.addClass('open');
            $cont.attr('style', '');
        }, 400);
    }
});

$(document).on('click', '#investors-more-info-close-button', function() {
    var navHeight = $('#navigation').outerHeight();
    $('html, body').animate({scrollTop: $('#investors').offset().top - navHeight}, 400);
    $('#investors-more-info-button').trigger('click');
});

/*******************************************************************************
 *  Three Steps more info 
 *******************************************************************************/
$(document).on('click', '#three-steps-control', function() {
    var $parent = $('#three-steps-container');
    var $cont = $('#three-steps-content');
    if($parent.hasClass('open'))
    {
        $(this).removeClass('pressed');
        $parent.css('height', $parent.css('height'));
        $parent.css('height'); /* This line must be here!! */
        $parent.removeClass('no-transition');
        $parent.removeClass('open');
        $parent.attr('style', '');
        
        var navHeight = $('#navigation').outerHeight();
        $('html, body').animate({scrollTop: $('#three-steps-container').offset().top - navHeight}, 400);
        var $topThree = $('#hideBoxOnMobile');
        $topThree.removeClass('openTree');
        $topThree.addClass('notOpenTree');
    }
    else
    {
        $(this).addClass('pressed');
        $parent.addClass('opening');
        var $topThree = $('#hideBoxOnMobile');
        $topThree.addClass('openTree');
        $topThree.removeClass('notOpenTree');
       // $parent.attr('data-height', $parent.css('height'));
        $parent.css('height', ($cont.height() - 10) + 'px');
        window.setTimeout(function() {
            $parent.addClass('no-transition');
            $parent.removeClass('opening');
            $parent.addClass('open');
            $parent.attr('style', '');
        }, 400);
    }
});

/*******************************************************************************
 *  Services
 *******************************************************************************/
$(document).on('click', '.services-back-button', function() {
    $('html, body').stop().animate({scrollTop: $('#services').offset().top - $('#navigation').outerHeight()}, 400);
});

/*
function closeServices() {
    $this = $('#service-pages');
    if($this.hasClass('open'))
    {
        $this.css('height', $this.outerHeight() + 'px');
        $this.css('height');
        $this.removeClass('no-transition');
        $this.addClass('closing');
        window.setTimeout(function() {
            $('.services-close').removeClass('open');
            $this.removeClass('closing');
            $this.removeClass('open');
            $this.attr('style', '');
            $('.service-button.active').removeClass('active');
            $this.children('.service-page.active').removeClass('active');
            $('html, body').stop().delay(400).animate({scrollTop: $('#serviceMenu').offset().top - $('#navigation').outerHeight()}, 400);
        }, 400);
        return 400;
    }
    return 0;
}

$(document).on('click', '.service-button', function() {
    var $this = $(this);
    var service = '#service-' + $this.attr('data-service');
    var $service = $(service);
    var $services = $('#service-pages');
    var active = $service.hasClass('active');
    window.setTimeout(function() {

        if(active)
            return;
        
        $('.services-close').addClass('open');
        $this.toggleClass('active');
        $service.addClass('active');
        $services.addClass('opening');
        $services.css('height', $service.outerHeight() + 25 + 'px');
        window.setTimeout(function() {
            $services.addClass('no-transition');
            $services.removeClass('opening');
            $services.addClass('open');
            $services.attr('style', '');
            $('html, body').stop().animate({scrollTop: $('#service-pages').offset().top - $('#navigation').outerHeight()}, 400);
        }, 400);
    }, closeServices());
    
});

$(document).on('click', '.services-close', function() {
    closeServices();
});
*/
// MODAL VALIDATE
