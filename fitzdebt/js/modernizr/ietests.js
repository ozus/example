Modernizr.addTest('ie9', function() {
    if (navigator.appName === 'Microsoft Internet Explorer' &&
        navigator.userAgent.indexOf('MSIE 9') > -1) 
    {
       return true;
    }
    return false;
});

Modernizr.addTest('ie8', function() {
    if (navigator.appName === 'Microsoft Internet Explorer' &&
        navigator.userAgent.indexOf('MSIE 8') > -1) 
    {
       return true;
    }
    return false;
});