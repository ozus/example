<?php
//na varijable se pozivaš kao na primjer
//echo $data['sell'];
//za sliju staviš <img src="cid:logo" height="63" width="250">
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Fitzgerald Debt Acquisitions LLC- Mortgage Information Worksheet</title>
    </head>
    <body style="margin: 0; padding: 0;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="padding: 10px 0 30px 0;">
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" style="border: 1px solid #cccccc; border-collapse: collapse;">
                        <tr>
                            <td bgcolor="#ffffff" >
                                <table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="100%"  style="border-bottom: 1px solid #cccccc;">
                                    <tr>
                                        <td width="300" align="center" style=" color: #666666; font-family: Arial, sans-serif; font-weight:bold; font-size:18px; padding:20px 0px 20px 0px;">
                                            <img src="cid:logo" height="63" width="250"><br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h1 style="text-align:center;color: #666666; font-family: Arial, sans-serif; font-weight:bold; font-size:24px;border-bottom:1px solid #cccccc;padding:0px 0px 20px 0px;margin:0;">Mortgage Information Worksheet</h1>
                                        </td>
                                    </tr>
                                    <tr>
                                            <td width="100%" align="left" style="background-color:#876b9e;padding:14px 0px 14px 14px; color: #fff; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; border-bottom:1px solid #cccccc;">
                                                        Contact Information:
                                            </td>
                                    </tr>
                                    <tr>
                                        <td bgcolor="#ffffff" >
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td width="240" align="left" style="padding:14px 0px 14px 14px; color: #282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; border-bottom:1px solid #cccccc;">
                                                        Note Holder's Name:
                                                    </td>
                                                    <td width="460" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px; border-bottom:1px solid #cccccc;">
                                                        <?php echo $data['fitz_modalForm_name']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="240" align="left" style="padding:14px 0px 14px 14px; color: #282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; border-bottom:1px solid #cccccc;">
                                                        Contact Telephone:
                                                    </td>
                                                    <td width="460" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px; border-bottom:1px solid #cccccc;">
                                                        <?php echo $data['fitz_modalForm_phone']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="240" align="left" style="padding:14px 0px 14px 14px; color: #282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; border-bottom:1px solid #cccccc;">
                                                       Email Address:
                                                    </td>
                                                    <td width="460" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px; border-bottom:1px solid #cccccc;">
                                                        <a href="<?php echo $data['fitz_modalForm_mail']; ?>"><?php echo $data['fitz_modalForm_mail']; ?></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="240" align="left" style="padding:14px 0px 14px 14px; color:#282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; border-bottom:1px solid #cccccc;">
                                                        Contact Fax:
                                                    </td>
                                                    <td width="460" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px; border-bottom:1px solid #cccccc;">
                                                        <?php echo $data['fitz_modalForm_fax']; ?>
                                                    </td>
                                                </tr>
                                    <tr>
                                            <td align="left" style="background-color:#876b9e;padding:14px 0px 14px 14px; color: #fff; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; border-bottom:1px solid #cccccc;">
                                                        Mortgage Information Worksheet
                                            </td>
                                            <td align="left" style="background-color:#876b9e;padding:14px 0px 14px 14px; color: #fff; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; border-bottom:1px solid #cccccc;"></td>
                                    </tr>


                                                <tr>
                                                    <td width="240" align="left" style="padding:14px 0px 14px 14px; color: #282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; border-bottom:1px solid #cccccc;">
                                                        Original Property Selling Price:
                                                    </td>
                                                    <td width="460" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px; border-bottom:1px solid #cccccc;">
                                                        <?php echo $data['fitz_modalForm_sellingPrice']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="240" align="left" style="padding:14px 0px 14px 14px; color: #282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; border-bottom:1px solid #cccccc;">
                                                        Down Payment:
                                                    </td>
                                                    <td width="460" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px; border-bottom:1px solid #cccccc;">
                                                        <?php echo $data['fitz_modalForm_downPayment']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="240" align="left" style="padding:14px 0px 14px 14px; color: #282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; border-bottom:1px solid #cccccc;">
                                                       Original Note Balance:
                                                    </td>
                                                    <td width="460" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px; border-bottom:1px solid #cccccc;">
                                                        <?php echo $data['fitz_modalForm_noteBalance']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="240" align="left" style="padding:14px 0px 14px 14px; color:#282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; border-bottom:1px solid #cccccc;">
                                                        Current Balance:
                                                    </td>
                                                    <td width="460" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px; border-bottom:1px solid #cccccc;">
                                                        <?php echo $data['fitz_modalForm_currentBalance']; ?>
                                                    </td>
                                                </tr>



                                                <tr>
                                                    <td width="240" align="left" style="padding:14px 0px 14px 14px; color: #282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; border-bottom:1px solid #cccccc;">
                                                        Months Financed (Amortization):
                                                    </td>
                                                    <td width="460" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px; border-bottom:1px solid #cccccc;">
                                                        <?php echo $data['fitz_modalForm_amortization']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="240" align="left" style="padding:14px 0px 14px 14px; color: #282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; border-bottom:1px solid #cccccc;">
                                                        Interest Rate:
                                                    </td>
                                                    <td width="460" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px; border-bottom:1px solid #cccccc;">
                                                        <?php echo $data['fitz_modalForm_interestRate']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="240" align="left" style="padding:14px 0px 14px 14px; color: #282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; border-bottom:1px solid #cccccc;">
                                                        Select Fixed or Variable Rate:
                                                    </td>
                                                    <td width="460" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px; border-bottom:1px solid #cccccc;">
                                                        <?php echo $data['fitz_modalForm_variableOrFixed']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="240" align="left" style="padding:14px 0px 14px 14px; color:#282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; border-bottom:1px solid #cccccc;">
                                                        Number of Payments Made:
                                                    </td>
                                                    <td width="460" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px; border-bottom:1px solid #cccccc;">
                                                        <?php echo $data['fitz_modalForm_paymentsMade']; ?>
                                                    </td>
                                                </tr>



                                                <tr>
                                                    <td width="240" align="left" style="padding:14px 0px 14px 14px; color: #282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; border-bottom:1px solid #cccccc;">
                                                        Payments Remaining:
                                                    </td>
                                                    <td width="460" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px; border-bottom:1px solid #cccccc;">
                                                        <?php echo $data['fitz_modalForm_paymentsRemaining']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="240" align="left" style="padding:14px 0px 14px 14px; color: #282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; border-bottom:1px solid #cccccc;">
                                                        Is there a Balloon with this Note?:
                                                    </td>
                                                    <td width="460" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px; border-bottom:1px solid #cccccc;">
                                                        <?php echo $data['fitz_modalForm_balloonNote']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="240" align="left" style="padding:14px 0px 14px 14px; color: #282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; border-bottom:1px solid #cccccc;">
                                                        If "Yes", What is the amount:
                                                    </td>
                                                    <td width="460" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px; border-bottom:1px solid #cccccc;">
                                                        <?php echo $data['fitz_modalForm_amountOfNote']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="240" align="left" style="padding:14px 0px 14px 14px; color:#282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; border-bottom:1px solid #cccccc;">
                                                        If "Yes", What is the Balloon Date?:
                                                    </td>
                                                    <td width="460" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px; border-bottom:1px solid #cccccc;">
                                                        <?php echo $data['fitz_modalForm_BalloonDate']; ?>
                                                    </td>
                                                </tr>



                                                <tr>
                                                    <td width="240" align="left" style="padding:14px 0px 14px 14px; color: #282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; border-bottom:1px solid #cccccc;">
                                                        Date of the Note's First Payment:
                                                    </td>
                                                    <td width="460" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px; border-bottom:1px solid #cccccc;">
                                                        <?php echo $data['fitz_modalForm_dateFirstNote']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="240" align="left" style="padding:14px 0px 14px 14px; color: #282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; border-bottom:1px solid #cccccc;">
                                                        Is It Current?:
                                                    </td>
                                                    <td width="460" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px; border-bottom:1px solid #cccccc;">
                                                        <?php echo $data['fitz_modalForm_current']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="240" align="left" style="padding:14px 0px 14px 14px; color: #282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; border-bottom:1px solid #cccccc;">
                                                        Lien Position of the Note:
                                                    </td>
                                                    <td width="460" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px; border-bottom:1px solid #cccccc;">
                                                        <?php echo $data['fitz_modalForm_notesPosition']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="240" align="left" style="padding:14px 0px 14px 14px; color:#282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; border-bottom:1px solid #cccccc;">
                                                        Property's Appraised Value:
                                                    </td>
                                                    <td width="460" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px; border-bottom:1px solid #cccccc;">
                                                        <?php echo $data['fitz_modalForm_appraisedValue']; ?>
                                                    </td>
                                                </tr>



                                                <tr>
                                                    <td width="240" align="left" style="padding:14px 0px 14px 14px; color: #282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; border-bottom:1px solid #cccccc;">
                                                        Appraisal Date:
                                                    </td>
                                                    <td width="460" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px; border-bottom:1px solid #cccccc;">
                                                        <?php echo $data['fitz_modalForm_AppraisalDate']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="240" align="left" style="padding:14px 0px 14px 14px; color: #282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; border-bottom:1px solid #cccccc;">
                                                        Is there a R/E Title Policy:
                                                    </td>
                                                    <td width="460" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px; border-bottom:1px solid #cccccc;">
                                                        <?php echo $data['fitz_modalForm_titlePolicy']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="240" align="left" style="padding:14px 0px 14px 14px; color: #282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; border-bottom:1px solid #cccccc;">
                                                        Property is Currently...:
                                                    </td>
                                                    <td width="460" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px; border-bottom:1px solid #cccccc;">
                                                        <?php echo $data['fitz_modalForm_statusOfProperty']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="240" align="left" style="padding:14px 0px 14px 14px; color:#282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; border-bottom:1px solid #cccccc;">
                                                        Property Description:
                                                    </td>
                                                    <td width="460" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px; border-bottom:1px solid #cccccc;">
                                                        <?php echo $data['fitz_modalForm_propertyDescrition']; ?>
                                                    </td>
                                                </tr>


                                                <tr>
                                                    <td width="240" align="left" style="padding:14px 0px 14px 14px; color: #282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; border-bottom:1px solid #cccccc;">
                                                        Additional Property Information, including age, condition, neighborhood, etc.:
                                                    </td>
                                                    <td width="460" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px; border-bottom:1px solid #cccccc;">
                                                        <?php echo $data['fitz_modalForm_additionalInformation']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="240" align="left" style="padding:14px 0px 14px 14px; color: #282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; border-bottom:1px solid #cccccc;">
                                                        Credit of Payor:
                                                    </td>
                                                    <td width="460" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px; border-bottom:1px solid #cccccc;">
                                                        <?php echo $data['fitz_modalForm_creditPayor']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="240" align="left" style="padding:14px 0px 14px 14px; color: #282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; border-bottom:1px solid #cccccc;">
                                                        Is the Payor in the Armed Services:
                                                    </td>
                                                    <td width="460" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px; border-bottom:1px solid #cccccc;">
                                                        <?php echo $data['fitz_modalForm_armedServices']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="240" align="left" style="padding:14px 0px 14px 14px; color:#282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; border-bottom:1px solid #cccccc;">
                                                        If "Yes" is the Payor deployed?:
                                                    </td>
                                                    <td width="460" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px; border-bottom:1px solid #cccccc;">
                                                        <?php echo $data['fitz_modalForm_payorDeployed']; ?>
                                                    </td>
                                                </tr>



                                                <tr>
                                                    <td width="240" align="left" style="padding:14px 0px 14px 14px; color: #282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; border-bottom:1px solid #cccccc;">
                                                        Amount of Cash You Require:
                                                    </td>
                                                    <td width="460" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px; border-bottom:1px solid #cccccc;">
                                                        <?php echo $data['fitz_modalForm_cashRequire']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="240" align="left" style="padding:14px 0px 14px 14px; color: #282828; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold;">
                                                        Do You Wish to Sell...:
                                                    </td>
                                                    <td width="460" align="left" style="padding:14px 5px 14px 0px; color: #363636; font-family: Arial, sans-serif; font-size: 14px;">
                                                        <?php echo $data['fitz_modalForm_sell']; ?>
                                                    </td>
                                                </tr>

                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                    </tr>
                                </table>
                                <tr>
                                    <td bgcolor="#313131">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="center" style="background-color:#876b9e;padding:14px 0px 14px 14px; font-size:9px; font-family:Arial, sans-serif; color:#cccccc;">
                                                    Copyright <strong style="color:#fff; font-weight:bold;">Fitzgerald Debt Acquisitions LLC&#8482;&nbsp;&copy; </strong>, <?php echo date('Y'); ?>.
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                </table>
                            </td>
                        </tr>
        </table>
    </body>
</html>
