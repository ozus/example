
<?php $id = get_page_id( 'About Us' ); ?>

<div id="about-us" class="section">
    <div class="container">
        <div class="row about-us-content">
            <div class="col-xs-7 col-sm-7 col-md-8 about-us-text-box">
                <div class="about-us-main-text">
                    <h2><?php the_field( 'about_us_title', $id ); ?></h2>
                     <p>
                        <?php the_field( 'about_us_content', $id ); ?>
                    </p>
                    <button id="about-us-more-info-button" type="button" class="button">More Info<span></span> </button>
                </div>
            </div>
            <div class="col-xs-5 col-sm-5 col-md-4 about-us-picture-box">
                <?php $image_about_us = get_field( 'about_us_content_image', $id ); ?>
                <img class="about-us-picture" src="<?php echo $image_about_us['url']; ?>" alt="" />
            </div>
        </div>
        <div id="about-us-more-info-container" class="more-info">
            <div id="about-us-more-content" class="more-info-inner">
                <div class="more-divider simple"></div>
                 <div class="testimonials-wrapper underlined-box">
                    <a class="testimonials-control left" href="javascript:void(0);"><span class="sprite left"></span></a>
                    <a class="testimonials-control right" href="javascript:void(0);"><span class="sprite right"></span></a>
                    <div id="testimonials-slider" class="testimonial-box owl-carousel">
                         <?php
                             if( have_rows( 'about_us_testimonials_repeater', $id ) ) :
                                 while( have_rows( 'about_us_testimonials_repeater', $id ) ) :
                                      the_row(); ?>
                                      <div class="testimonial-item">
                                          <div class="row">
                                              <div class="col-sm-3">  
                                                   <?php $image_testimonial = get_sub_field( 'about_us_testimonial_repeater_image', $id); ?>
                                                   <img src="<?php echo $image_testimonial['url']; ?>" alt="" />
                                              </div>    
                                              <div class="col-sm-9">
                                                   <p>
                                                       <?php the_sub_field( 'about_us_testimonial_repeater_text', $id ); ?>
                                                   </p>
                                                   <h4><?php the_sub_field( 'about_us_testimonial_repeater_to', $id ); ?></h4>
                                              </div>
                                          </div>
                                      </div>    
                                 <?php endwhile; ?>
                             <?php endif; ?>
                    </div>
                </div>
                <h2><?php the_field( 'about_us_team_title', $id ); ?></h2>
                <div class="row team-box">
                    <?php if( have_rows( 'about_us_team_repeater', $id ) ) :
                        while( have_rows( 'about_us_team_repeater', $id ) ) :
                              the_row(); ?>
                              <div class="col-sm-6">
                                  <div class="imgBoxPart">
                                      <?php $image_team = get_sub_field( 'about_us_team_repeater_image', $id ); ?>
                                      <img src="<?php echo $image_team['url']; ?>" class="team-img" alt="" />
                                  </div>
                                  <div class="txtBoxPart">
                                  <h4><?php the_sub_field( 'about_us_team_repeater_name', $id ); ?></h4>
                                  <p>
                                      <?php the_sub_field( 'about_us_team_repeater_desc', $id ); ?>    
                                  </p>
                                  </div>
                              </div>  
                        <?php endwhile; 
                    endif; ?>
                </div>
                <div class="more-divider simple"></div>
                <h2><?php the_field( 'about_us_chose_us_title', $id ); ?></h2>
                <div class="row choose-us-box">
                    <?php the_field( 'about_us_choose_us_content', $id ); ?>
                </div>
                <div class="underlined-box about-us-extra-info-box">
                    <h3><?php the_field( 'about_us_extra_title', $id ); ?></h3>
                    <p><?php the_field( 'about_us_extra_content', $id ); ?></p>
                </div>
                <button id="about-us-more-info-close-button" type="button" class="button">Close <i class="sprite miniarrowup"></i></button>
            </div>
        </div>
    </div>
    <div class="parallax-container">
        <div class="parallax parallax1"></div>
        <div class="parallax-content three-steps">
            <div class="container">
                <div id="three-steps-wrapper" class="row">
                    <div id="three-steps-container" class="">
                        <div id="three-steps-content">
                            <h2><?php the_field( 'about_us_succes_title', $id ); ?></h2>
                            <div class="clearfix">
                            <?php if( have_rows( 'about_us_succes_repeater', $id ) ) :
                                 while( have_rows( 'about_us_succes_repeater', $id ) ) :
                                      the_row(); ?>
                                <div id="hideBoxOnMobile" class="">
                                <div class="col-xs-4">
                                    <div class="success-ball strategy-ball">
                                        <?php $image_succes = get_sub_field( 'about_us_succes_repeater_image', $id ); ?>
                                        <img src="<?php echo $image_succes['url']; ?> " class="success-ball-img" alt="" />
                                    </div>
                                    <h4><?php the_sub_field('about_us_succes_repeater_name', $id ); ?></h4>
                                </div>    
                                </div>
                                 <?php endwhile; ?>
                             <?php endif; ?>      
                                 <?php if( have_rows( 'about_us_succes_repeater', $id ) ) :
                                 while( have_rows( 'about_us_succes_repeater', $id ) ) :
                                      the_row(); ?>
                                 <div class="col-sm-4 col-xs-12">
                                    <div class="showOnMobile">
                                        <div class="success-ball mission-ball">
                                            <img src="<?php $image_succes['url']; ?> " class="success-ball-img" alt="" />
                                        </div>
                                        <h4><?php the_sub_field( 'about_us_succes_repeater_name', $id ); ?></h4>
                                    </div>
                                    <div class="success-info">
                                        <p>
                                            <?php the_sub_field( 'about_us_succes_repeater_desc', $id ); ?>
                                        </p>
                                    <b></b>
                                    </div>
                                </div>    
                                 <?php endwhile; ?>
                             <?php endif; ?>   
                            </div>
                        </div>
                    </div>
                    <a href="javascript:void(0);" id="three-steps-control">
                        <span class="click-to-open">Click here for more information <i class="sprite miniarrowdown"></i></span>
                        <span class="click-to-close">Close <i class="sprite miniarrowup"></i></span>
                    </a>
                </div>
            </div>
        </div>        
    </div>
</div>