<?php
/*
  Template Name: Form Page
 */
?><?php

function checkRadioButton($data, $name, $value) {
    return ($data[$name] == $value)? ' checked="checked"' : '';
}

function selectSelectOption($data, $name, $value) {
    return ($data[$name] == $value)? ' selected="selected"' : '';
}

if ($_REQUEST['fitz_modalForm_form'] === 'true'):
    $sanitize_conditions = array(
        'fitz_modalForm_name' => array('filter' => FILTER_SANITIZE_STRING),
        'fitz_modalForm_phone' => array('filter' => FILTER_SANITIZE_NUMBER_INT),
        'fitz_modalForm_mail' => array('filter' => FILTER_SANITIZE_EMAIL),
        'fitz_modalForm_fax' => array('filter' => FILTER_SANITIZE_NUMBER_INT),
        'fitz_modalForm_sellingPrice' => array('filter' => FILTER_SANITIZE_STRING),
        'fitz_modalForm_downPayment' => array('filter' => FILTER_SANITIZE_NUMBER_INT),
        'fitz_modalForm_noteBalance' => array('filter' => FILTER_SANITIZE_NUMBER_INT),
        'fitz_modalForm_currentBalance' => array('filter' => FILTER_SANITIZE_NUMBER_INT),
        'fitz_modalForm_amortization' => array('filter' => FILTER_SANITIZE_NUMBER_INT),
        'fitz_modalForm_interestRate' => array('filter' => FILTER_SANITIZE_NUMBER_INT),
        'fitz_modalForm_variableOrFixed' => array('filter' => FILTER_SANITIZE_STRING),
        'fitz_modalForm_paymentsMade' => array('filter' => FILTER_SANITIZE_NUMBER_INT),
        'fitz_modalForm_paymentsRemaining' => array('filter' => FILTER_SANITIZE_NUMBER_INT),
        'fitz_modalForm_balloonNote' => array('filter' => FILTER_SANITIZE_STRING),
        'fitz_modalForm_amountOfNote' => array('filter' => FILTER_SANITIZE_STRING),
        'fitz_modalForm_BalloonDate' => array('filter' => FILTER_SANITIZE_STRING),
        'fitz_modalForm_dateFirstNote' => array('filter' => FILTER_SANITIZE_STRING),
        'fitz_modalForm_current' => array('filter' => FILTER_SANITIZE_STRING),
        'fitz_modalForm_notesPosition' => array('filter' => FILTER_SANITIZE_STRING),
        'fitz_modalForm_appraisedValue' => array('filter' => FILTER_SANITIZE_STRING),
        'fitz_modalForm_AppraisalDate' => array('filter' => FILTER_SANITIZE_STRING),
        'fitz_modalForm_titlePolicy' => array('filter' => FILTER_SANITIZE_STRING),
        'fitz_modalForm_statusOfProperty' => array('filter' => FILTER_SANITIZE_STRING),
        'fitz_modalForm_propertyDescrition' => array('filter' => FILTER_SANITIZE_STRING),
        'fitz_modalForm_additionalInformation' => array('filter' => FILTER_SANITIZE_STRING),
        'fitz_modalForm_creditPayor' => array('filter' => FILTER_SANITIZE_STRING),
        'fitz_modalForm_armedServices' => array('filter' => FILTER_SANITIZE_STRING),
        'fitz_modalForm_payorDeployed' => array('filter' => FILTER_SANITIZE_STRING),
        'fitz_modalForm_cashRequire' => array('filter' => FILTER_SANITIZE_STRING),
        'fitz_modalForm_sell' => array('filter' => FILTER_SANITIZE_STRING)/*,
        'fitz_modalForm_signedNote' => array('filter' => FILTER_SANITIZE_STRING),
        'fitz_modalForm_SettlementStatement' => array('filter' => FILTER_SANITIZE_STRING),
        'fitz_modalForm_MortgageTrustDeed' => array('filter' => FILTER_SANITIZE_STRING)*/
    );

    global $data;
    $data = filter_var_array($_POST['data'], $sanitize_conditions);
    //var_dump($data);

    //returning quotes
    foreach ($data as $key => $value) {
        $data[$key] = htmlspecialchars_decode($data[$key], ENT_QUOTES);
    }

    //validating email or whatever
    $validate_conditions = array(
        'fitz_modalForm_mail' => array('filter' => FILTER_VALIDATE_EMAIL)
    );
    $data = array_merge($data, filter_var_array($data, $validate_conditions));
    
    //list or required variables
    $required = array(
        'fitz_modalForm_name' => 'Holder\'s Name',
        'fitz_modalForm_phone' => 'Contact Telephone Number',
        'fitz_modalForm_mail' => 'Email Address',
        'fitz_modalForm_fax' => 'Contact Fax Number',
        'fitz_modalForm_sellingPrice' => 'Original Property Selling Price',
        'fitz_modalForm_downPayment' => 'Down Payment Number',
        'fitz_modalForm_noteBalance' => 'Original Note Balance Number',
        'fitz_modalForm_currentBalance' => 'Current Balance Number',
        'fitz_modalForm_amortization' => 'Months Financed (Amortization) Number',
        'fitz_modalForm_interestRate' => 'Interest Rate Number',
        'fitz_modalForm_variableOrFixed' => 'Variable or Fixed Rate',
        'fitz_modalForm_paymentsMade' => 'Number of Payments Made Number',
        'fitz_modalForm_paymentsRemaining' => 'Payments Remaining Number',
        'fitz_modalForm_balloonNote' => 'Ballon Note',
        'fitz_modalForm_amountOfNote' => 'Amount of Balloon Note',
        'fitz_modalForm_BalloonDate' => 'Date of Balloon Note',
        'fitz_modalForm_dateFirstNote' => 'Note First Date',
        'fitz_modalForm_current' => 'Is Current Note',
        'fitz_modalForm_notesPosition' => 'Note\'s Position',
        'fitz_modalForm_appraisedValue' => 'Property\'s Appraised Value',
        'fitz_modalForm_AppraisalDate' => 'Appraisal Date',
        'fitz_modalForm_titlePolicy' => 'R/E Title Policy',
        'fitz_modalForm_statusOfProperty' => 'Property Status',
        'fitz_modalForm_propertyDescrition' => 'Property Description',
        'fitz_modalForm_additionalInformation' => 'Additional Information',
        'fitz_modalForm_creditPayor' => 'Credit of Payor',
        'fitz_modalForm_armedServices' => 'Payor Armed Services',
        'fitz_modalForm_payorDeployed' => 'Is Payor Deployed',
        'fitz_modalForm_cashRequire' => 'Required Cash',
        'fitz_modalForm_sell' => 'Sell Status'/*,
        'fitz_modalForm_signedNote' => 'Signed Note Copy',
        'fitz_modalForm_SettlementStatement' => 'Settlement Statement Copy',
        'fitz_modalForm_MortgageTrustDeed' => 'Mortgage/Trust Deed Copy'*/
    );

    //checking if something is missing
    $message = '';
    foreach ($required as $name => $value) {
        //echo $name . ': ' . $data[$name] . '<br />';
        if($data['fitz_modalForm_balloonNote'] == 'no')
        {
            if($name == 'fitz_modalForm_amountOfNote' || $name == 'fitz_modalForm_BalloonDate') {
                continue;
            }
        }
        if($data['fitz_modalForm_armedServices'] == 'no')
        {
            if($name == 'fitz_modalForm_payorDeployed') {
                continue;
            }
        }
        
        if ($data[$name] == '' || $data[$name] == false) {
            $message.=$value . ' is required. <br/>';
        }
    }
    
    if (!$message) {

        //initalizing phpmailer
        require TEMPLATEPATH . '/phpmailer/PHPMailerAutoload.php';
        $mail = new PHPMailer;
        $mail->FromName = 'Fitzgerald Debt Acquisitions, LLC Web Form';
        $mail->Subject = 'Fitzgerald Debt Acquisitions, LLC Web Form';

        //adding addresses
        //$mail->addAddress('jhartman@fitzdebt.com', 'Jeff Hartman');
        //$mail->addAddress('zvidakov@gmail.com', 'Zoran');
        $mail->addAddress('marko@odindesign.com', 'Marko');
        $mail->addAddress('bruno@odindesign.com', 'Bruno');

        //building message
        $mail->isHTML(true);

        //cid works in buffering fine, no globals needed
        $mail->AddEmbeddedImage(TEMPLATEPATH . '/images/fitz.png', 'logo', 'fitz.png');

        //attachemnt files
        foreach ($_FILES['file']['name'] as $key => $value) {
            move_uploaded_file($_FILES['file']['tmp_name'][$key], TEMPLATEPATH . '/uploads/' . $_FILES['file']['name'][$key]);
            $mail->AddAttachment(TEMPLATEPATH . '/uploads/' . $_FILES['file']['name'][$key]);
        }

        //checking for template
        if (is_file(TEMPLATEPATH . '/mortgage-acquisition-template.php')) {
            ob_start();
            include TEMPLATEPATH . '/mortgage-acquisition-template.php';
            $mail->Body = ob_get_clean();
        }

        //sending mail
        if ($mail->send()) {
            //header('Refresh: 5; URL=' . home_url());
            //echo '<!DOCTYPE html><html><head><meta http-equiv="refresh" content="5;url=' . home_url() . '"></head><body>Thank you for sending query. You will be redicted in 5 seconds.</body></html>';
            // Do this to prevent send on refresh and also because automatic redirection didn't work for me ~MV
            header('Location: ' . the_permalink() . '?success=true');
        }

        exit();
    }
endif;
?>

<?php get_header(); ?>

<div id="single" class="black">
    <div class="" id="modalForm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title purple" id="myModalLabel">Request Quote</h4><a href="java script:window.close();"><button class="button formButtonModal">Back</button></a>
                </div>
                <form action="" method="POST" enctype="multipart/form-data">
                    
                    <div id="contact-response"></div>
                    
                    <?php 
                        if($message) { 
                            echo '<div class="alert alert-danger" style="margin: 20px;">' . $message . '</div>';
                        }
                        elseif(isset($_GET['success']) && $_GET['success'] == true) { 
                            echo '<div class="alert alert-success" style="margin: 20px;">Your query has been sent. You can now close this form.</div>';
                        }
                    ?>
                    <div class="modal-body clearfix" id="modalFormBody">
                        <h4 class="purple">Your Contact Information</h4>
                        <hr>
                        <div class="col-md-6 col-xs-12 form-group">
                            <label for="fitz_modalForm_name">Note Holder's Name</label>
                            <input class="text-input form-control" id="fitz_modalForm_name" name="data[fitz_modalForm_name]" placeholder="Note Holder's Name" type="text" value="<?php echo $data['fitz_modalForm_name']; ?>" />
                        </div>
                        <div class="col-md-6 col-xs-12 form-group">
                            <label for="fitz_modalForm_phone">Contact Telephone</label>
                            <input class="text-input form-control" id="fitz_modalForm_phone"  name="data[fitz_modalForm_phone]" placeholder="Contact Telephone" type="tel" value="<?php echo $data['fitz_modalForm_phone']; ?>" />
                        </div>

                        <div class="col-md-6 col-xs-12 form-group">
                            <label for="fitz_modalForm_mail">Email Address</label>
                            <input class="text-input form-control" id="fitz_modalForm_mail" name="data[fitz_modalForm_mail]" placeholder="Email Address" type="email" value="<?php echo $data['fitz_modalForm_mail']; ?>" />
                        </div>
                        <div class="col-md-6 col-xs-12 form-group">
                            <label for="fitz_modalForm_fax">Contact Fax</label>
                            <input class="text-input form-control" id="fitz_modalForm_fax"  name="data[fitz_modalForm_fax]" placeholder="Contact Fax" type="tel" value="<?php echo $data['fitz_modalForm_fax']; ?>" />
                        </div>

                        <h4 class="purple">Mortgage Information Worksheet</h4>
                        <hr>
                        <div class="col-md-6 col-xs-12 form-group">
                            <label for="fitz_modalForm_sellingPrice">Original Property Selling Price</label>
                            <input class="text-input form-control" id="fitz_modalForm_sellingPrice" name="data[fitz_modalForm_sellingPrice]" placeholder="Original Property Selling Price" type="text" value="<?php echo $data['fitz_modalForm_sellingPrice']; ?>" />
                        </div>
                        <div class="col-md-6 col-xs-12 form-group">
                            <label for="fitz_modalForm_downPayment">Down Payment</label>
                            <input class="text-input form-control" id="fitz_modalForm_downPayment"  name="data[fitz_modalForm_downPayment]" placeholder="Down Payment" type="text" value="<?php echo $data['fitz_modalForm_downPayment']; ?>" />
                        </div>

                        <div class="col-md-6 col-xs-12 form-group">
                            <label for="fitz_modalForm_noteBalance">Original Note Balance</label>
                            <input class="text-input form-control" id="fitz_modalForm_noteBalance" name="data[fitz_modalForm_noteBalance]" placeholder="Original Note Balance" type="text" value="<?php echo $data['fitz_modalForm_noteBalance']; ?>" />
                        </div>
                        <div class="col-md-6 col-xs-12 form-group">
                            <label for="fitz_modalForm_currentBalance">Current Balance</label>
                            <input class="text-input form-control" id="fitz_modalForm_currentBalance"  name="data[fitz_modalForm_currentBalance]" placeholder="Current Balance" type="text" value="<?php echo $data['fitz_modalForm_currentBalance']; ?>" />
                        </div>

                        <div class="col-md-6 col-xs-12 form-group">
                            <label for="fitz_modalForm_amortization">Months Financed (Amortization)</label>
                            <input class="text-input form-control" id="fitz_modalForm_amortization" name="data[fitz_modalForm_amortization]" placeholder="Months Financed (Amortization)" type="text" value="<?php echo $data['fitz_modalForm_amortization']; ?>" />
                        </div>
                        <div class="col-md-6 col-xs-12 form-group">
                            <label for="fitz_modalForm_interestRate">Interest Rate</label>
                            <input class="text-input form-control" id="fitz_modalForm_interestRate"  name="data[fitz_modalForm_interestRate]" placeholder="Interest Rate" type="text" value="<?php echo $data['fitz_modalForm_interestRate']; ?>" />
                        </div>


                        <div class="col-sm-12 form-group checkboxes">
                            <label for="fitz_modalForm_variableOrFixed" class="control-label input-group">Select Fixed or Variable Rate</label>
                            <div class="btn-group" data-toggle="buttons"id="fitz_modalForm_variableOrFixed">
                                <label class="btn btn-default">
                                    <input name="data[fitz_modalForm_variableOrFixed]" id="" value="fixed Mortgage" type="radio" <?php echo checkRadioButton($data, 'fitz_modalForm_variableOrFixed', 'fixed Mortgage') ?>>Fixed Mortgage
                                </label>
                                <label class="btn btn-default">
                                    <input name="data[fitz_modalForm_variableOrFixed]" id="" value="variable / Adjustable Mortgage" type="radio" <?php echo checkRadioButton($data, 'fitz_modalForm_variableOrFixed', 'variable / Adjustable Mortgage') ?>>Variable / Adjustable Mortgage
                                </label>
                            </div>
                        </div>

                        <div class="col-md-6 col-xs-12 form-group">
                            <label for="fitz_modalForm_paymentsMade">Number of Payments Made</label>
                            <input class="text-input form-control" id="fitz_modalForm_paymentsMade" name="data[fitz_modalForm_paymentsMade]" placeholder="Number of Payments Made" type="text" value="<?php echo $data['fitz_modalForm_paymentsMade']; ?>" />
                        </div>
                        <div class="col-md-6 col-xs-12 form-group">
                            <label for="fitz_modalForm_paymentsRemaining">Payments Remaining</label>
                            <input class="text-input form-control" id="fitz_modalForm_paymentsRemaining"  name="data[fitz_modalForm_paymentsRemaining]" placeholder="Payments Remaining" type="text" value="<?php echo $data['fitz_modalForm_paymentsRemaining']; ?>" />
                        </div>

                        <div class="col-sm-12 form-group checkboxes">
                            <label for="fitz_modalForm_balloonNote" class="control-label input-group">Is there a Balloon with this Note?</label>
                            <div class="btn-group" data-toggle="buttons" id="fitz_modalForm_balloonNote">
                                <label class="btn btn-default">
                                    <input name="data[fitz_modalForm_balloonNote]" id="fitz_ifYes" value="yes" type="radio" <?php echo checkRadioButton($data, 'fitz_modalForm_balloonNote', 'yes') ?>>Yes
                                </label>
                                <label class="btn btn-default">
                                    <input name="data[fitz_modalForm_balloonNote]" id="fitz_ifNo" value="no" type="radio" <?php echo checkRadioButton($data, 'fitz_modalForm_balloonNote', 'no') ?>>No
                                </label>
                            </div>
                        </div>

                        <!-- IF YES =======================================================-->
                        <div class="ifYes clearfix">
                            <div id="amountOfNote" class="col-md-6 col-xs-12 form-group">
                                <label for="fitz_modalForm_amountOfNote">If "Yes", What is the amount</label>
                                <input class="text-input form-control" id="fitz_modalForm_amountOfNote" name="data[fitz_modalForm_amountOfNote]" placeholder="What is the amount" type="text" value="<?php echo $data['fitz_modalForm_amountOfNote']; ?>" />
                            </div>
                            <div id="balloonDate" class="col-md-6 col-xs-12 form-group">
                                <label for="fitz_modalForm_BalloonDate">If "Yes", What is the Balloon Date?</label>
                                <input class="text-input dataForm" id="fitz_modalForm_BalloonDate"  name="data[fitz_modalForm_BalloonDate]" placeholder="What is the Balloon Date?" type="text" value="<?php echo $data['fitz_modalForm_BalloonDate']; ?>" />
                            </div>
                        </div>
                        <!-- END IF YES =======================================================-->

                        <script type="text/javascript">
                            $('#fitz_ifYes, #fitz_ifNo').on('change', function() {
                                $yes = $('.ifYes');
                                if ($(this).prop('checked') && $(this).val() === 'yes') {
                                    $yes.css('display', 'block');
                                }
                                else {
                                    $yes.attr('style', '');
                                }
                            });
                        </script>

                        <div class="col-sm-6 col-xs-12 form-group" class="first-payment-date">
                            <label for="fitz_modalForm_dateFirstNote" class="">Date of the Note's First Payment</label>
                            <input type="text" class="text-input dataForm" id="fitz_modalForm_dateFirstNote" name="data[fitz_modalForm_dateFirstNote]" placeholder="Date of the Note's First Payment" value="<?php echo $data['fitz_modalForm_dateFirstNote']; ?>" />
                            
                        </div>

                        <div class="col-sm-6 col-xs-12 form-group checkboxes alignGroup">
                            <label for="fitz_modalForm_current" class="control-label input-group">Is It Current?</label>
                            <div class="btn-group" data-toggle="buttons" id="fitz_modalForm_current">
                                <label class="btn btn-default">
                                    <input name="data[fitz_modalForm_current]" id="" value="yes" type="radio" <?php echo checkRadioButton($data, 'fitz_modalForm_current', 'yes') ?>>Yes
                                </label>
                                <label class="btn btn-default">
                                    <input name="data[fitz_modalForm_current]" id="" value="no" type="radio" <?php echo checkRadioButton($data, 'fitz_modalForm_current', 'no') ?>>No
                                </label>
                            </div>
                        </div>

                        <div class="col-sm-12 form-group checkboxes">
                            <label for="fitz_modalForm_notesPosition" class="control-label input-group">Lien Position of the Note</label>
                            <div class="btn-group" data-toggle="buttons" id="fitz_modalForm_notesPosition">
                                <label class="btn btn-default" id="">
                                    <input name="data[fitz_modalForm_notesPosition]" id="" value="First Position" type="radio" <?php echo checkRadioButton($data, 'fitz_modalForm_notesPosition', 'First Position') ?>>First Position
                                </label>
                                <label class="btn btn-default">
                                    <input name="data[fitz_modalForm_notesPosition]" id="" value="Second Position" type="radio" <?php echo checkRadioButton($data, 'fitz_modalForm_notesPosition', 'Second Position') ?>>Second Position
                                </label>
                                <label class="btn btn-default">
                                    <input name="data[fitz_modalForm_notesPosition]" id="" value="Third Position" type="radio" <?php echo checkRadioButton($data, 'fitz_modalForm_notesPosition', 'Third Position') ?>>Third Position
                                </label>
                            </div>
                        </div>

                        <div class="col-md-6 col-xs-12 form-group">
                            <label for="fitz_modalForm_appraisedValue">Property's Appraised Value</label>
                            <input class="text-input form-control" id="fitz_modalForm_appraisedValue" name="data[fitz_modalForm_appraisedValue]" placeholder="Property's Appraised Value" type="text" value="<?php echo $data['fitz_modalForm_appraisedValue']; ?>" />
                        </div>
                        <div class="col-md-6 col-xs-12 form-group">
                            <label for="fitz_modalForm_AppraisalDate">Appraisal Date</label>
                            <input class="text-input dataForm" id="fitz_modalForm_AppraisalDate"  name="data[fitz_modalForm_AppraisalDate]" placeholder="Appraisal Date" type="text" value="<?php echo $data['fitz_modalForm_AppraisalDate']; ?>" />
                        </div>

                        <div class="col-sm-6 col-xs-12 form-group checkboxes">
                            <label for="fitz_modalForm_titlePolicy" class="control-label input-group">Is there a R/E Title Policy</label>
                            <div class="btn-group" data-toggle="buttons" id="fitz_modalForm_titlePolicy">
                                <label class="btn btn-default">
                                    <input name="data[fitz_modalForm_titlePolicy]" id="" value="yes" type="radio" <?php echo checkRadioButton($data, 'fitz_modalForm_titlePolicy', 'yes') ?>>Yes
                                </label>
                                <label class="btn btn-default">
                                    <input name="data[fitz_modalForm_titlePolicy]" id="" value="no" type="radio" <?php echo checkRadioButton($data, 'fitz_modalForm_titlePolicy', 'no') ?>>No
                                </label>
                            </div>
                        </div>

                        <div class="col-sm-6 col-xs-12 form-group checkboxes">
                            <label for="fitz_modalForm_statusOfProperty" class="control-label input-group">Property is Currently...</label>
                            <div class="btn-group" data-toggle="buttons" id="fitz_modalForm_statusOfProperty">
                                <label class="btn btn-default">
                                    <input name="data[fitz_modalForm_statusOfProperty]" id="" value="ownerOccupied " type="radio" <?php echo checkRadioButton($data, 'fitz_modalForm_statusOfProperty', 'ownerOccupied') ?>>Owner Occupied
                                </label>
                                <label class="btn btn-default">
                                    <input name="data[fitz_modalForm_statusOfProperty]" id="" value="rental" type="radio" <?php echo checkRadioButton($data, 'fitz_modalForm_statusOfProperty', 'rental') ?>>Rental
                                </label>
                                <label class="btn btn-default">
                                    <input name="data[fitz_modalForm_statusOfProperty]" id="" value="vacant" type="radio" <?php echo checkRadioButton($data, 'fitz_modalForm_statusOfProperty', 'vacant') ?>>Vacant
                                </label>
                            </div>
                        </div>

                        <div class="dropdown form-group">
                            <div class="col-sm-2">
                                <label for="fitz_modalForm_propertyDescrition" class="control-label input-group">Property Description</label>
                            </div>
                            <div class="col-sm-4">
                                <select class="form-control" name="data[fitz_modalForm_propertyDescrition]" id="fitz_modalForm_propertyDescrition">
                                    <option value="Single Family" <?php echo selectSelectOption($data, 'fitz_modalForm_propertyDescrition', 'Single Family') ?>>Single Family</option>
                                    <option value="Duplex" <?php echo selectSelectOption($data, 'fitz_modalForm_propertyDescrition', 'Duplex') ?>>Duplex</option>
                                    <option value="Triplex / Four-plex" <?php echo selectSelectOption($data, 'fitz_modalForm_propertyDescrition', 'Triplex / Four-plex') ?>>Triplex / Four-plex</option>
                                    <option value="Commercial" <?php echo selectSelectOption($data, 'fitz_modalForm_propertyDescrition', 'Commercial') ?>>Commercial</option>
                                    <option value="Vacant Land" <?php echo selectSelectOption($data, 'fitz_modalForm_propertyDescrition', 'Vacant Land') ?>>Vacant Land</option>
                                    <option value="Special Use" <?php echo selectSelectOption($data, 'fitz_modalForm_propertyDescrition', 'Special Use') ?>>Special Use</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-12 form-group">
                            <label for="fitz_modalForm_additionalInformation" class="col-sm-12">Additional Property Information, including age, condition, neighborhood, etc.</label>
                            <textarea class="text-input form-control" id="fitz_modalForm_additionalInformation" name="data[fitz_modalForm_additionalInformation]" rows="6" placeholder="Additional Property Information, including age, condition, neighborhood, etc."><?php echo $data['fitz_modalForm_additionalInformation']; ?></textarea>
                        </div>

                        <div class="dropdown form-group">
                            <div class="col-sm-2 crediti">
                                <label for="fitz_modalForm_creditPayor" class="control-label input-group">Credit of Payor</label>
                            </div>
                            <div class="col-sm-5">
                                <select class="form-control" id="fitz_modalForm_creditPayor" name="data[fitz_modalForm_creditPayor]">
                                    <option value="Excellent" <?php echo selectSelectOption($data, 'fitz_modalForm_creditPayor', 'Excellent') ?>>Excellent</option>
                                    <option value="Good" <?php echo selectSelectOption($data, 'fitz_modalForm_creditPayor', 'Good') ?>>Good</option>
                                    <option value="Fair" <?php echo selectSelectOption($data, 'fitz_modalForm_creditPayor', 'Fair') ?>>Fair</option>
                                    <option value="Poor" <?php echo selectSelectOption($data, 'fitz_modalForm_creditPayor', 'Poor') ?>>Poor</option>
                                    <option value="Unknown" <?php echo selectSelectOption($data, 'fitz_modalForm_creditPayor', 'Unknown') ?>>Unknown</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-6 col-xs-12 form-group checkboxes">
                            <label for="fitz_modalForm_armedServices" class="control-label input-group">Is the Payor in the Armed Services</label>
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default" id="fitz_modalForm_armedServices">
                                    <input name="data[fitz_modalForm_armedServices]" id="" value="yes" type="radio" <?php echo checkRadioButton($data, 'fitz_modalForm_armedServices', 'yes') ?>>Yes
                                </label>
                                <label class="btn btn-default">
                                    <input name="data[fitz_modalForm_armedServices]" id="" value="no" type="radio" <?php echo checkRadioButton($data, 'fitz_modalForm_armedServices', 'no') ?>>No
                                </label>
                            </div>
                        </div>

                        <div class="col-sm-6 col-xs-12 form-group checkboxes">
                            <label for="fitz_modalForm_payorDeployed" class="control-label input-group">If "Yes" is the Payor deployed?</label>
                            <div class="btn-group" data-toggle="buttons" >
                                <label class="btn btn-default" id="fitz_modalForm_payorDeployed">
                                    <input name="data[fitz_modalForm_payorDeployed]" id="" value="yes" type="radio" <?php echo checkRadioButton($data, 'fitz_modalForm_payorDeployed', 'yes') ?>>Yes
                                </label>
                                <label class="btn btn-default">
                                    <input name="data[fitz_modalForm_payorDeployed]" id="" value="no" type="radio" <?php echo checkRadioButton($data, 'fitz_modalForm_payorDeployed', 'no') ?>>No
                                </label>
                            </div>
                        </div>

                        <div class="col-md-5 col-xs-12 form-group">
                            <label for="fitz_modalForm_cashRequire">Amount of Cash You Require</label>
                            <input class="text-input form-control" id="fitz_modalForm_cashRequire" name="data[fitz_modalForm_cashRequire]" placeholder="Amount of Cash You Require" type="text" value="<?php echo $data['fitz_modalForm_cashRequire']; ?>" />
                        </div>

                        <div class="col-md-7 col-xs-12 form-group">
                            <div class="col-sm-5">
                                <label for="fitz_modalForm_sell" class="wish control-label input-group">Do You Wish to Sell...</label>
                            </div>
                            <div class="col-sm-7">
                                <select class="form-control" id="fitz_modalForm_sell" name="data[fitz_modalForm_sell]">
                                    <option value="The Entire Note" <?php echo selectSelectOption($data, 'fitz_modalForm_sell', 'The Entire Note') ?>>The Entire Note </option>
                                    <option value="A Number of Future Payments (partial)" <?php echo selectSelectOption($data, 'fitz_modalForm_sell', 'A Number of Future Payments (partial)') ?>>A Number of Future Payments (partial)</option>
                                    <option value="The Balloon" <?php echo selectSelectOption($data, 'fitz_modalForm_sell', 'The Balloon') ?>>The Balloon</option>
                                    <option value="Give Me Some Options" <?php echo selectSelectOption($data, 'fitz_modalForm_sell', 'Give Me Some Options') ?>>Give Me Some Options</option>
                                </select>
                            </div>
                        </div>
                        <div class="fileInput form-group col-sm-12">
                            <div class="col-sm-4 col-xs-12">
                                <label for="fitz_modalForm_signedNote">Copy of Signed Note</label>
                                <input id="fitz_modalForm_signedNote" name="file[fitz_modalForm_signedNote]" placeholder="Copy of Signed Note" type="file" />
                            </div>

                            <div class="col-sm-4 col-xs-12">
                                <label for="fitz_modalForm_SettlementStatement">Copy of Settlement Statement</label>
                                <input id="fitz_modalForm_SettlementStatement" name="file[fitz_modalForm_SettlementStatement]" placeholder="Copy of Settlement Statement" type="file" />
                            </div>

                            <div class="col-sm-4 col-xs-12">
                                <label for="fitz_modalForm_MortgageTrustDeed">Copy of Mortgage/Trust Deed</label>
                                <input id="fitz_modalForm_MortgageTrustDeed" name="file[fitz_modalForm_MortgageTrustDeed]" placeholder="Copy of Mortgage/Trust Deed" type="file" />
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a onclick="self.close();"><button type="button" class="button" data-dismiss="modal">Back</button></a>
                        <input type="hidden" name="fitz_modalForm_form" value="true"/>
                        <input class="button" type="submit" value="Request Quote">
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <script type="text/javascript">
        
        jQuery(document).ready(function() {
            jQuery('input[type="radio"]').each(function() {
                if(jQuery(this).attr('checked'))
                    jQuery(this).parent('label').addClass('active');
            });
        });
    </script>
    <script type="text/javascript" src="<?php echo TEMPLATE_URI; ?>/js/main.js"></script>
</div>
<?php wp_footer(); ?>
</body>
</html>