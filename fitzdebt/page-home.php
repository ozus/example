<?php $id = get_page_id( 'Home' ); ?>
<div id="home" class="section">
    <div class="home-background">
        <div class="container">
            <div class="home-tags">                
                <div class="home-tag-1">
                    <?php the_field( 'home_page_title', $id ); ?>
                </div>
                <div class="home-tag-2">
                    <?php the_field( 'home_page_subtitle', $id ); ?>
                </div>
            </div>
            <div class="social-content">
                    <div class="fb-like" data-href="https://www.facebook.com/DebtSales.DebtPortfolios.DebtBuyers.DebtBrokers" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
                    <span><?php the_field( 'home_page_facebook', $id ); ?></span>
            </div>
        </div>
    </div>
</div>


