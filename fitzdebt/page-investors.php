
<?php $id = get_page_id( 'Investors' ); ?>

<div id="investors" class="section">
    <div class="container">
        <div class="row investors-content">
            <h2 class="col-xs-12 col-sm-12"><?php the_field( 'investors_page_title', $id ); ?></h2>
            <div class="col-xs-7 col-sm-7 investors-main-text">
                <p>
                   <?php the_field( 'investors_page_content', $id ); ?>
                </p>
                <button id="investors-more-info-button" type="button" class="button">More Info <span></span></button>
            </div>
            <?php $image_content = get_field( 'investors_page_image', $id ); ?>
            <div class="col-xs-5 col-sm-5 investors-picture-box">
                <img class="investors-picture" src="<?php echo $image_content['url']; ?>" alt="<?php $image_content['alt']; ?>" />
            </div>
        </div>
        <div id="investors-more-info-container" class="more-info">
            <div id="investors-more-content" class="more-info-inner">
                <div class="more-divider simple"></div>
                <p>
                <?php the_field( 'investors_page_more_content', $id ); ?>
                </p>
                <h2><?php the_field( 'investors_page_join_message', $id ); ?></h2>
                <?php 
                if( have_rows ('investors_page_repeater', $id) ):
                    while( have_rows( 'investors_page_repeater', $id) ) :
                         the_row(); ?>
                         <div class="investor-step">
                            <div class="step-body clearfix"> 
                                <div class="step-number"><?php the_sub_field( 'Investors_page_repeater_step' ); ?></div>
                                <div class="step-content"><?php the_sub_field( 'investors_page_repeater_content' ); ?></div>
                            </div>
                            <div class="more-divider"><div class="divider-circle"><span class="sprite miniarrowdown"></span></div></div>
                        </div>    
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
                <button id="investors-more-info-close-button" type="button" class="button">Close <i class="sprite miniarrowup"></i></button>
        </div>
    </div>
</div>
<div class="parallax-container">
        <div class="parallax parallax2"></div>
        <div class="parallax-content ">
            <div class="container investorsparallax">  
                <div class="contentParaInvest">
                    <h2><?php the_field( 'investors_page_bottom_message', $id ); ?></h2>
                    <?php $image_bottom = get_field( 'investors_page_bottom_image', $id ); ?>
                    <div class="round">
                        <img src="<?php echo $image_bottom['url']; ?>" alt="investors" />
                    </div>
                </div>
            </div>
        </div>        
</div>
