

<?php $id = get_page_id('Services'); ?>

<div id="services" class="section">
    <div class="container">
        <div class="">            
            <div id="services-slider" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="item active">
                        <h2><?php the_field( 'services_title', $id ); ?></h2>
                        <div id="serviceMenu" class="row">
                        <?php
                            if (have_rows('services_category_repeater1', $id)) :
                                while (have_rows('services_category_repeater1', $id)) :
                                    the_row();
                                    ?>
                                    <div class="col-sm-4 col-xs-6 serviceBox s-consumerassets">
                                        <?php $posts = get_sub_field('services_category_repeater_page1'); ?>
                                        <?php foreach ($posts as $p) : ?>
                                            <?php $i = 0; ?>
                                            <?php
                                            if (get_the_title($p->ID) == 'Service Page 1') {
                                                $i = 1;
                                            }
                                            if (get_the_title($p->ID) == 'Service Page 2') {
                                                $i = 2;
                                            }
                                            if (get_the_title($p->ID) == 'Service Page 3') {
                                                $i = 3;
                                            }
                                            if (get_the_title($p->ID) == 'Service Page 4') {
                                                $i = 4;
                                            }
                                            if (get_the_title($p->ID) == 'Service Page 5') {
                                                $i = 5;
                                            }
                                            if (get_the_title($p->ID) == 'Service Page 6') {
                                                $i = 6;
                                            }
                                            if (get_the_title($p->ID) == 'Service Page 7') {
                                                $i = 7;
                                            }
                                            if (get_the_title($p->ID) == 'Service Page 8') {
                                                $i = 8;
                                            }
                                            if (get_the_title($p->ID) == 'Service Page 9') {
                                                $i = 9;
                                            }
                                            if (get_the_title($p->ID) == 'Service Page 10') {
                                                $i = 10;
                                            }
                                            ?>
                                        <?php endforeach; ?>
                                            <a href="javascript:void(0);" title="" class="service-button" data-service="consumerassets" data-target="#services-slider" data-slide-to="<?php echo $i; ?>">
                                            <div class="alignBox">
                                                <i class="sprite <?php the_sub_field('services_repeater_category_sprite1'); ?>_p"></i>
                                                <div><?php the_sub_field('services_repater_category_title1'); ?></div> 
                                            </div>
                                        </a> 
                                    </div>    
                                <?php endwhile;
                            endif; 
                        ?>
                        </div>
                    </div>
                    <?php
                    if (have_rows('services_category_repeater1', $id)) :
                        while (have_rows('services_category_repeater1', $id)) :
                            the_row();
                            ?>            
                            <?php $posts = get_sub_field('services_category_repeater_page1'); ?>
                            <?php
                            foreach ($posts as $p) :
                                $i = 0;
                                if (get_the_title($p->ID) == 'Service Page 1') {
                                    $i = 1;
                                }
                                if (get_the_title($p->ID) == 'Service Page 2') {
                                    $i = 2;
                                }
                                if (get_the_title($p->ID) == 'Service Page 3') {
                                    $i = 3;
                                }
                                if (get_the_title($p->ID) == 'Service Page 4') {
                                    $i = 4;
                                }
                                if (get_the_title($p->ID) == 'Service Page 5') {
                                    $i = 5;
                                }
                                if (get_the_title($p->ID) == 'Service Page 6') {
                                    $i = 6;
                                }
                                if (get_the_title($p->ID) == 'Service Page 7') {
                                    $i = 7;
                                }
                                if (get_the_title($p->ID) == 'Service Page 8') {
                                    $i = 8;
                                }
                                if (get_the_title($p->ID) == 'Service Page 9') {
                                    $i = 9;
                                }
                                if (get_the_title($p->ID) == 'Service Page 10') {
                                    $i = 10;
                                }
                                ?>
                                <div class="item" data-id ="<?php echo $i; ?>" >
                                    <div id="service-commercialassets" class="service-page">
                                            <?php //Page title and subtitle  ?>
                                      <?php if( get_field( 'service_page_title', $p -> ID ) ) : ?>
                                        <h2><?php the_field('service_page_title', $p->ID); ?></h2>
                                      <?php endif; ?>
                                      <?php if( get_field( 'service_page_subbtitle', $p -> ID ) ) : ?> 
                                        <h3><?php the_field('service_page_subbtitle', $p->ID); ?></h3>
                                      <?php endif; ?>  
                                        <?php //Top buttons  ?>
                                        <?php if (get_field('service_page_buttons_top', $p->ID)) : ?> 
                                            <div class="buttonBoxMortgage">
                                                <?php if (in_array('Online form', get_field('service_page_buttons_top', $p->ID))) : ?>
                                                    <div><a class="button col-md-3 col-sm-3" href="<?php echo home_url(); ?>/form">By Online Form</a></div>
                                                <?php endif; ?>
                                                <?php if (in_array('Contact us', get_field('service_page_buttons_top', $p->ID))) : ?>
                                                    <div><a href="#contact"><button class="button col-md-3 col-md-offset-1 col-sm-3 col-sm-offset-1">By Contacting Us</button></a></div>
                                                <?php endif; ?>
                                                <?php if (in_array('Download our PDF form', get_field('service_page_buttons_top', $p->ID))) : ?>
                                                    <div><a href="forms/MortgageQuoteSheet.pdf" target="_blank"><button class="button col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1">By Downloading Our PDF Form</button></a></div>
                                            <?php endif; ?>
                                            </div>
                                        <?php else : ?>
                                        <?php endif; ?>
                                        <?php if (get_field('service_page_content_style', $p->ID)) : ?>
                                        <?php //page content text if no colums ?>
                                            <?php if (in_array('one column', get_field('service_page_content_style', $p->ID))) : ?>
                                                <?php if( get_field( 'services_page_top_text_boxed', $p -> ID ) ) : ?>
                                                    <?php if (in_array('with frame no columns', get_field( 'services_page_top_text_boxed', $p->ID))) : ?>
                                                         <div class="consultingListBox">
                                                    <?php endif; ?> 
                                                <?php else: ?>
                                                <?php endif; ?>
                                      <?php if( get_field( 'service_page_text_no', $p -> ID) ) : ?>                       
                                                             <p><?php the_field('service_page_text_no', $p->ID); ?></p>
                                      <?php endif; ?>                       
                                                 <?php if( get_field( 'services_page_top_text_boxed', $p -> ID ) ) : ?>    
                                                    <?php if (in_array('with frame no columns', get_field( 'services_page_top_text_boxed', $p->ID))) : ?>
                                                         </div>  
                                                    <?php endif; ?>
                                               <?php else : ?>
                                               <?php endif; ?>
                                            <?php endif; ?>
                                        <?php //page content text if colums  ?>
                                        <?php if (in_array('two columns', get_field('service_page_content_style', $p->ID))) : ?>
                                             <?php if( get_field( 'services_page_top_text_boxed', $p -> ID ) ) : ?>
                                                  <?php if (in_array('with frame two columns', get_field( 'services_page_top_text_boxed', $p->ID))) : ?>
                                                      <div class="consultingListBox">
                                                  <?php endif; ?> 
                                             <?php else: ?> 
                                             <?php endif; ?> 
                                      <?php if( get_field( 'service_page_text_two', $p -> ID ) ) : ?>                   
                                                        <div class="two-col">    
                                                            <p><?php the_field('service_page_text_two', $p->ID); ?></p>
                                                       </div>
                                                       <div class="clearfix"></div>
                                      <?php endif; ?>                 
                                             <?php if( get_field( 'services_page_top_text_boxed', $p -> ID ) ) : ?>          
                                                  <?php if (in_array('with frame two columns', get_field( 'services_page_top_text_boxed', $p->ID))) : ?>  
                                                      </div>
                                                  <?php endif; ?>
                                             <?php else : ?>
                                             <?php endif; ?>
                                        <?php endif; ?>
                                        <?php else: ?> 
                                        <?php endif; ?>     
                                        <?php //Page image ?>
                                      <?php if( get_field( 'service_page_repeater_image', $p -> ID ) ) : ?>  
                                        <?php
                                           if (have_rows('service_page_repeater_image', $p->ID)) :
                                             while (have_rows('service_page_repeater_image', $p->ID)) :
                                                the_row();
                                        ?>
                                                <?php $image_content = get_sub_field('service_page_repeater_image_select', $p->ID); ?>
                                                <div class="col-xs-5 col-sm-5 investors-picture-box">
                                                    <img class="investors-picture" src="<?php echo $image_content['url']; ?>" alt="<?php $image_content['alt']; ?>" />
                                                </div>
                                                <?php endwhile;
                                            endif;
                                            ?>
                                      <?php endif; ?>  
                                        <?php //Middle page buttons ?>
                                        <?php if (get_field('service_page_button_middle', $p->ID)) : ?>  
                                          <div class="buttonBoxMortgage">
                                            </br>
                                            </br>
                                                <?php if (in_array('Online form', get_field('service_page_button_middle', $p->ID))) : ?>
                                                    <div><a class="button col-md-3 col-sm-3" href="<?php echo home_url(); ?>/form-page">By Online Form</a></div>
                                                <?php endif; ?>
                                                <?php if (in_array('Contact us', get_field('service_page_button_middle', $p->ID))) : ?>
                                                    <div><a href="#contact"><button class="button col-md-3 col-md-offset-1 col-sm-3 col-sm-offset-1">By Contacting Us</button></a></div>
                                                <?php endif; ?>
                                                <?php if (in_array('Download our PDF form', get_field('service_page_button_middle', $p->ID))) : ?>
                                                    <div><a href="forms/MortgageQuoteSheet.pdf" target="_blank"><button class="button col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1">By Downloading Our PDF Form</button></a></div>
                                                <?php endif; ?>
                                          </div>
                                        <?php else : ?> 
                                        <?php endif; ?>
                                        <?php //Middle page title and subtitle ?>
                                        <center>
                                      <?php if( get_field( 'services_page_middle_title', $p -> ID ) ) : ?>     
                                            <h3 class="purple"><?php the_field('services_page_middle_title', $p->ID); ?></h3>
                                      <?php endif; ?>
                                      <?php if( get_field( 'service_page_middle_subb', $p -> ID) ) : ?>     
                                            <p><?php the_field('service_page_middle_subb', $p->ID); ?></p>
                                      <?php endif; ?>      
                                        </center>  
                                        <?php //Middle page text  ?>
                                        <?php if( get_field( 'services_page_middle_text_boxed', $p -> ID ) ) : ?> 
                                                <?php if( in_array( 'with frame', get_field( 'services_page_middle_text_boxed', $p -> ID ) ) ) : ?>
                                                        <div class="consultingListBox">
                                                <?php endif; ?>
                                        <?php else : ?>                    
                                        <?php endif; ?>
                                      <?php if( get_field( 'service_page_middle_text', $p -> ID ) ) : ?>                    
                                                           <p><?php the_field('service_page_middle_text', $p->ID); ?></p>
                                      <?php endif; ?>                     
                                        <?php if( get_field( 'services_page_middle_text_boxed', $p -> ID ) ) : ?>                   
                                                <?php  if( in_array( 'with frame', get_field( 'services_page_middle_text_boxed', $p -> ID ) ) ) : ?>
                                                        </div>
                                                <?php endif; ?>
                                        <?php else : ?>
                                        <?php endif; ?>        
                                        <?php //Page repeater if nesessary for some content that need to be added repeatetly  ?>
                                        <?php if( get_field( 'service_page_repeater_style', $p -> ID ) ) : ?>
                                        <?php if( in_array( 'In 2 or more rows( 2 per row )', get_field( 'service_page_repeater_style', $p -> ID ) ) ) : ?>
                                      <?php if( get_field( 'service_page_repeater', $p -> ID ) ) : ?> 
                                        <center><div class="serviceBoxPage">
                                                 <?php if( have_rows( 'service_page_repeater', $p -> ID ) ) :
                                                         while( have_rows( 'service_page_repeater', $p -> ID ) ):
                                                                the_row(); ?>
                                                                <div class="col-md-6 col-xs-12">
                                                                   <div class="contentServiceBox">
                                                                       <h3 class="purple"><?php the_sub_field( 'service_page_repeater_title' ); ?></h3>
                                                                       <p><?php the_sub_field( 'service_page_repeater_text' ); ?></p>
                                                                   </div>
                                                                   <div class="imgBoxPart">
                                                                      <?php $image_repeater = get_sub_field( 'service_page_repeater_image' ); ?>
                                                                      <img src="<?php echo $image_repeater['url']; ?>" class="team-img" alt="" />
                                                                   </div>
                                                                </div>
                                                          <?php endwhile;
                                                       endif; ?>
                                        </div></center>
                                      <?php endif; ?> 
                                        <?php endif; ?>
                                        <?php if(in_array( 'In one row', get_field( 'service_page_repeater_style', $p -> ID ) ) ) : ?>
                                      <?php if( get_field( 'service_page_repeater', $p -> ID ) ) : ?> 
                                                <div class="row">
                                                   <?php if( have_rows( 'service_page_repeater', $p -> ID ) ) : 
                                                           while( have_rows( 'service_page_repeater', $p -> ID ) ) :
                                                               the_row(); ?>
                                                               <div class="col-sm-4 commercialBox">
                                                                  <p class="purple"><?php the_sub_field( 'service_page_repeater_title' ); ?></p>
                                                                  <div clas="commercial-list">
                                                                      <?php the_sub_field( 'service_page_repeater_text' ); ?>
                                                                  </div>
                                                               </div> 
                                                           <?php endwhile; 
                                                   endif; ?>
                                                </div>
                                      <?php endif; ?>  
                                        <?php endif; ?>
                                        <?php endif; ?>
                                        <?php //bottom page title and content  ?>
                                      <?php if( get_field( 'service_page_bottom_title', $p -> ID ) ) : ?>  
                                        <h3 class="purple"><?php the_field('service_page_bottom_title', $p->ID); ?></h3>
                                      <?php endif; ?>  
                                        <?php if( get_field( 'services_page_bottom_text_boxed', $p ->ID ) ) : ?>
                                             <?php if( in_array( 'with frame', get_field( 'services_page_bottom_text_boxed', $p -> ID ) ) ) : ?>
                                                  <div class="consultingListBox">
                                             <?php endif; ?>
                                        <?php else : ?>              
                                        <?php endif; ?>
                                      <?php if( get_field( 'service_page_bottom_text', $p -> ID ) ) : ?>                
                                                     <p><?php the_field('service_page_bottom_text', $p->ID); ?>:</p>
                                      <?php endif; ?>               
                                        <?php if( get_field( 'services_page_bottom_text_boxed', $p ->ID ) ) : ?>
                                              <?php if( in_array( 'with frame', get_field( 'services_page_bottom_text_boxed', $p -> ID ) ) ) : ?> 
                                                  </div>
                                              <?php endif; ?>
                                        <?php else : ?>
                                        <?php endif; ?>
                                        <?php //Page bottom buttons  ?>
                                        <?php if (get_field('service_page_button_bottom', $p->ID)) : ?>  
                                           <div class="buttonBoxMortgage">
                                               <?php if (in_array('Online form', get_field('service_page_button_bottom', $p->ID))) : ?>
                                                    <div><a class="button col-md-3 col-sm-3" href="<?php echo home_url(); ?>/form">By Online Form</a></div>
                                                <?php endif; ?>
                                                <?php if (in_array('Contact us', get_field('service_page_button_bottom', $p->ID))) : ?>
                                                    <div><a href="#contact"><button class="button col-md-3 col-md-offset-1 col-sm-3 col-sm-offset-1">By Contacting Us</button></a></div>
                                                <?php endif; ?>
                                                <?php if (in_array('Download our PDF form', get_field('service_page_button_bottom', $p->ID))) : ?>
                                                    <div><a href="forms/MortgageQuoteSheet.pdf" target="_blank"><button class="button col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1">By Downloading Our PDF Form</button></a></div>
                                               <?php endif; ?>
                                           </div>
                                        <?php else : ?>
                                        <?php endif; ?>
                                        <div class="services-back-box">
                                            <button type="button" class="button services-back-button" data-target="#services-slider" data-slide-to="0">Back</button>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php endwhile;
                    endif; 
                    ?> 
                </div>    
            </div>
        </div>    
    </div>
    <div class="parallax-container">
        <div class="parallax parallax3"></div>
        <div class="parallax-content">
            <div class="container">
                <div class="">
                    <h2><?php the_field('services_bottom_title', $id); ?></h2>
                    <div class="row boxHowContainer">
                        <!-- BOX 1 -->
                       <?php
                           if (have_rows('services_repeater_work_steps', $id)) :
                              while (have_rows('services_repeater_work_steps', $id)) :
                                   the_row();
                       ?>
                                   <div class="kinky-box-wrapper col-xs-4">
                                       <div class="kinky-number"><?php the_sub_field('services_repeater_work_steps_number'); ?></div>
                                           <div class="kinky-container">
                                              <div class="kinky-body">
                                                 <h3><?php the_sub_field('services_repeater_work_steps_title'); ?></h3>
                                                  <p><?php the_sub_field('services_repeater_work_steps_desc'); ?></p>
                                              </div>
                                           </div>
                                   </div>     
                              <?php endwhile; 
                           endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
