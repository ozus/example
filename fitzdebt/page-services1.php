
<?php $id = get_page_id( 'Services' ); ?>
<div id="services" class="section">
<div class="container">
<div class="">



<div class="carousel-inner">
    <div class="item active">
    <h2><?php the_field( 'services_title', $id ); ?></h2>
                        <div id="serviceMenu" class="row">
                        <?php if( have_rows( 'services_category_repeater1', $id ) ) :
                            while(have_rows( 'services_category_repeater1', $id ) ) :
                                 the_row(); ?>
                                         <?php $posts = get_sub_field( 'services_category_repeater_page1' ); ?>
                            
                                         <?php 
                                         foreach( $posts as $post ) :
                                         setup_postdata( $post );
                                         $fields = get_fields();
                                                 var_dump( $fields ); 
                                         if( $post -> post_title == 'Service Page 1') {
                                             $i == 1;
                                         }
                                         if( $post -> post_title == 'Service Page 2' ) {
                                             $i == 2;
                                         }
                                         if( $post -> post_title == 'Service Page 3' ) {
                                             $i == 3;
                                         }
                                         if( $post -> post_title == 'Service Page 4' ) {
                                             $i == 4;
                                         }
                                         if( $post -> post_title == 'Service Page 5' ) {
                                             $i == 5;
                                         }
                                         if( $post -> post_title  == 'Service Page 6' ) {
                                             $i == 6;
                                         }
                                         if( $post -> post_title  == 'Service Page 7' ) {
                                             $i == 7;
                                         }
                                         if( $post -> post_title == 'Service Page 8' ) {
                                             $i == 8;
                                         }
                                         if( $post -> post_title  == 'Service Page 9' ) {
                                             $i == 9;
                                         }
                                         if( $post -> post_title == 'Service Page 10' ) {
                                             $i == 10;
                                         }
                                         
                                         ?>
                                         
                                         <a href="javascript:void(0);" title="" class="service-button" data-service="consumerassets" data-target="#services-slider" data-slide-to="1">
                                           
                                             <div class="alignBox">
                                                
                                                <i class="sprite <?php the_sub_field( 'services_repeater_category_sprite1' ); ?>_p"></i>
                                                <div><?php the_sub_field( 'services_repater_category_title1' ); ?></div> 
                                            </div>
                                         </a>   
                                         <?php endforeach; ?>
                            <?php wp_reset_postdata(); ?>
                            <?php endwhile; ?>
                        <?php endif; ?>
                        </div>
</div> 
</div>    
<div id="seriesCarousel" class="carousel slide">
    <div class ="carusel-inner" >         
         <div id="services-slider" class="carousel slide" data-ride="carousel">
              <?php if( have_rows( 'services_category_repeater1', $id ) ) :
                            while(have_rows( 'services_category_repeater1', $id ) ) :
                                 the_row(); ?>            
                                         <?php $posts = get_sub_field( 'services_category_repeater_page1' ); ?>
                                         <?php 
                                         foreach( $posts as $post ) :
                                         setup_postdata( $post );
                                         
                                         if( $post -> post_title == 'Service Page 1') {
                                             $i == 1;
                                         }
                                         if( $post -> post_title == 'Service Page 2' ) {
                                             $i == 2;
                                         }
                                         if( $post -> post_title == 'Service Page 3' ) {
                                             $i == 3;
                                         }
                                         if( $post -> post_title == 'Service Page 4' ) {
                                             $i == 4;
                                         }
                                         if( $post -> post_title == 'Service Page 5' ) {
                                             $i == 5;
                                         }
                                         if( $post -> post_title  == 'Service Page 6' ) {
                                             $i == 6;
                                         }
                                         if( $post -> post_title  == 'Service Page 7' ) {
                                             $i == 7;
                                         }
                                         if( $post -> post_title == 'Service Page 8' ) {
                                             $i == 8;
                                         }
                                         if( $post -> post_title  == 'Service Page 9' ) {
                                             $i == 9;
                                         }
                                         if( $post -> post_title == 'Service Page 10' ) {
                                             $i == 10;
                                         }
                                         
                                         ?>
                        
                                        <div class="item" data-id ="1" >
                                            <?php 
                                                 echo $post -> post_content;
                                                 echo $post -> post_title;  ?>
                                                 
                                        </div>
                                        <?php endforeach; ?>
                                        <?php wp_reset_postdata(); ?>
                            <?php endwhile;
               endif; ?>              
         </div>
      </div> 
   
</div>



</div>
</div>
      <div class="parallax-container">
        <div class="parallax parallax3"></div>
        <div class="parallax-content">
            <div class="container">
                <div class="">
                    <h2>How it works:</h2>
                    <div class="row boxHowContainer">
                    <!-- BOX 1 -->
                        <div class="kinky-box-wrapper col-xs-4">
                            <div class="kinky-number">1</div>
                            <div class="kinky-container">
                                <div class="kinky-body">
                                    <h3>Identifying a debt portfolio.</h3>
                                    <p>Choosing a portfolio that is right for you is one of the most important factor in the success of your business.</p>
                                </div>
                            </div>
                        </div>
                    <!-- BOX 2 -->
                        <div class="kinky-box-wrapper col-xs-4">
                        <div class="kinky-number">2</div>
                        <div class="kinky-container">
                            <div class="kinky-body">
                            <h3>Marketing your debt portfolio. </h3>
                                <p>Our acquired debt portfolio is only as valuable as the target market dictates. Let Fitzgerald help you select which marketing plan is the most effective way to realize maximum gains.</p>
                            </div>
                        </div>
                        </div>
                    <!-- BOX 3 -->
                        <div class="kinky-box-wrapper col-xs-4">
                            <div class="kinky-number">3</div>
                            <div class="kinky-container">
                                <div class="kinky-body">
                                <h3>Management &amp; Processing.</h3>
                                    <p>We offer a wide array of full and complete services designed to streamline processing and reduce management overhead associated with every level of the buying business.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    