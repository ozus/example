<?php
/**
 * Template Name: Service Creator
 */
?>

<?php acf_form_head(); ?>
<?php get_header(); ?>

     <?php while ( have_posts() ) : the_post(); ?>

                <?php acf_form(array(
					'post_id'	=> get_page_id( 'Service_test' ),
					'post_title'	=> false,
					'submit_value'	=> 'Insert data for Services page!'
	        )); ?>
     <?php endwhile; ?>
  


<?php if( have_rows( 'services_category_repeater1') ) :
        while( have_rows( 'services_category_repeater1') ) :
             the_row(); ?>

             <?php
             $posts = get_sub_field( 'services_category_repeater_page1' ); ?>
           
            <?php if( $posts ) :  ?>      
           
       
           <?php foreach( $posts as $post ) : 
              setup_postdata( $post );  
         
          
              if( $post-> post_title == 'Service Page 1' ) :
                 
                   acf_form_head(); 
                 
                         acf_form( array( 
                                 'post_id'	=> get_page_id( 'Service Page 1' ),
	                         'post_title'	=> false,
	                         'submit_value'	=> 'Update Service Page 1',
                                 'updated_message' => 'Service page 1 was created or updated'
                         ));  
              endif;      
              if( $post-> post_title == 'Service Page 2' ) :
                 acf_form_head(); 
                 
                         acf_form( array( 
                                 'post_id'	=> get_page_id( 'Service Page 2' ),
	                         'post_title'	=> false,
	                         'submit_value'	=> 'Update Service Page 2',
                                 'updated_message' => 'Service page 2 was created or updated'
                         )); 
                 
             endif;
             if( $post-> post_title == 'Service Page 3' ) :
                acf_form_head(); 
                 
                         acf_form( array( 
                                 'post_id'	=> get_page_id( 'Service Page 3' ),
	                         'post_title'	=> false,
	                         'submit_value'	=> 'Update Service Page 3',
                                 'updated_message' => 'Service page 3 was created or updated'
                         )); 
             endif;
             if( $post-> post_title == 'Service Page 4' ) :
                 acf_form_head(); 
                  
                        acf_form( array( 
                                 'post_id'	=> get_page_id( 'Service Page 4' ),
	                         'post_title'	=> false,
	                         'submit_value'	=> 'Update Service Page 4',
                                 'updated_message' => 'Service page 4 was created or updated'
                         )); 
             endif;
             if( $post-> post_title == 'Service Page 5' ) :
                 acf_form_head(); 
                 
                         acf_form( array( 
                                 'post_id'	=> get_page_id( 'Service Page 5' ),
	                         'post_title'	=> false,
	                         'submit_value'	=> 'Update Service Page 5',
                                 'updated_message' => 'Service page 5 was created or updated'
                         )); 
             endif;
             if( $post-> post_title == 'Service Page 6' ) :
                acf_form_head(); 
                 
                         acf_form( array( 
                                 'post_id'	=> get_page_id( 'Service Page 6' ),
	                         'post_title'	=> false,
	                         'submit_value'	=> 'Update Service Page 6',
                                 'updated_message' => 'Service page 6 was created or updated'
                         )); 
             endif;
             if( $post-> post_title == 'Service Page 7' ) :
                acf_form_head(); 
                 
                         acf_form( array( 
                                 'post_id'	=> get_page_id( 'Service Page 7' ),
	                         'post_title'	=> false,
	                         'submit_value'	=> 'Update Service Page 7',
                                 'updated_message' => 'Service page 7 was created or updated'
                         )); 
             endif;
             if( $post-> post_title == 'Service Page 8' ) :
               acf_form_head(); 
                 
                         acf_form( array( 
                                 'post_id'	=> get_page_id( 'Service Page 8' ),
	                         'post_title'	=> false,
	                         'submit_value'	=> 'Update Service Page 8',
                                 'updated_message' => 'Service page 8 was created or updated'
                         )); 
             endif;
             if( $post-> post_title == 'Service Page 9' ) :
                acf_form_head(); 
                 
                         acf_form( array( 
                                 'post_id'	=> get_page_id( 'Service Page 9' ),
	                         'post_title'	=> false,
	                         'submit_value'	=> 'Update Service Page 9',
                                 'updated_message' => 'Service page 9 was created or updated'
                         )); 
             endif;
             if( $post-> post_title == 'Service Page 10' ) :
                acf_form_head(); 
                 
                         acf_form( array( 
                                 'post_id'	=> get_page_id( 'Service Page 10' ),
	                         'post_title'	=> false,
	                         'submit_value'	=> 'Update Service Page 10',
                                 'updated_message' => 'Service page 10 was created or updated'
                         )); 
             endif;  ?>
         
          <?php endforeach; ?>
        <?php wp_reset_postdata(); ?>  
      <?php endif; ?>
     
 <?php endwhile;
endif; ?>  

    

<?php get_footer(); ?>

               
                