<?php
/**
 * Template Name: 3th Service Form
 */
?>

<?php acf_form_head(); ?>
<?php get_header(); ?>
<?php get_template_part( 'service_forms/services_forms_navigation' ); ?>
<?php while( have_posts() ) : the_post(); ?>
          <?php acf_form( array( 
               'post_id'	=> get_page_id( 'Service Page 3' ),
	       'post_title'	=> false,
	       'submit_value'	=> 'Update Service Page 3',
               'updated_message' => 'Service page 3 was created or updated'
           )); ?>
          <?php endwhile; ?>
<?php get_template_part( 'service_forms/services_forms_navigation' ); ?>
<?php get_footer(); ?>

